package prober

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	reconnectDelay = 3 * time.Second
	streamTimeout  = 2 * time.Minute

	// Exported metrics.
	connections = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "connections",
			Help: "Number of connections",
		},
		[]string{"stream", "status"},
	)
	connected = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "stream_connected",
			Help: "Is the prober connected to the stream.",
		},
		[]string{"stream"},
	)
	bytesReceived = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "stream_bytes_received",
			Help: "Bytes received",
		},
		[]string{"stream"},
	)
	decodingErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "stream_decode_errors",
			Help: "Decoding errors.",
		},
		[]string{"stream"},
	)
	streamErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "stream_errors",
			Help: "Number of times the connection was broken.",
		},
		[]string{"stream"},
	)
	streamRMS = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "stream_rms",
			Help: "RMS amplitude of the stream audio (dB).",
		},
		[]string{"stream"},
	)
	streamPeak = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "stream_peak",
			Help: "Peak amplitude of the stream audio (dB).",
		},
		[]string{"stream"},
	)
)

func init() {
	prometheus.MustRegister(
		connections,
		connected,
		bytesReceived,
		decodingErrors,
		streamRMS,
		streamPeak,
	)
}

// Analyze one second of audio at a time.
const bufSize = 44100

// Prober continuously listens to a stream and exports some metrics
// related to connectivity and audio quality. If the connection fails,
// it will try to reconnect forever, without a timeout.
type Prober struct {
	streamURL  string
	streamName string
}

// New returns a new unstarted Prober.
func New(streamURL string) (*Prober, error) {
	u, err := url.Parse(streamURL)
	if err != nil {
		return nil, err
	}
	return &Prober{
		streamURL:  streamURL,
		streamName: u.Path,
	}, nil

}

// Receive a stream content over HTTP. We're using the standard Go
// client as it can handle this case, no need to hijack the connection.
func (p *Prober) stream(deadline time.Time) {
	resp, err := http.Get(p.streamURL)
	if err != nil {
		log.Printf("connection error: %v", err)
		connections.WithLabelValues(p.streamName, "error").Inc()
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Printf("HTTP status %d", resp.StatusCode)
		connections.WithLabelValues(p.streamName, "http_error").Inc()
		return
	}

	// Instrument the raw bytes transfered, before decoding happens.
	r := newInstrumentedReader(resp.Body, bytesReceived.WithLabelValues(p.streamName))
	dec, err := newDecoder(r, filepath.Ext(p.streamURL))
	if err != nil {
		log.Printf("decoder error: %v", err)
		connections.WithLabelValues(p.streamName, "decoder_error").Inc()
		return
	}

	log.Printf("connected to %s", resp.Request.URL.String())
	connected.WithLabelValues(p.streamName).Set(1)
	connections.WithLabelValues(p.streamName, "ok").Inc()

	// Read the data one buffer at a time (which corresponds to 1
	// second of audio), compute some metrics and export them.
	fr := &floatReader{dec}
	buf := make([]float64, bufSize)
	for time.Now().Before(deadline) {
		if err := fr.ReadFloats(buf); err != nil {
			streamErrors.WithLabelValues(p.streamName).Inc()
			break
		}

		stats := analyze(buf)
		streamRMS.WithLabelValues(p.streamName).Set(stats.rms)
		streamPeak.WithLabelValues(p.streamName).Set(stats.peak)
	}

	if err := dec.Close(); err != nil {
		log.Printf("decode error: %v", err)
	}

	// Reset stream analysis values for convenience, so we don't
	// have to join with 'stream_connected' every time.
	connected.WithLabelValues(p.streamName).Set(0)
	streamRMS.WithLabelValues(p.streamName).Set(noSignal)
	streamPeak.WithLabelValues(p.streamName).Set(noSignal)
}

// Run the Prober forever.
func (p *Prober) Run() {
	for {
		p.stream(time.Now().Add(streamTimeout))

		// Limit the number of outgoing connections by
		// sleeping a bit before retrying.
		time.Sleep(reconnectDelay)
	}
}

// An instrumentedReader wraps an io.Reader and exports the number of
// bytes read through it to a prometheus.Counter.
type instrumentedReader struct {
	io.Reader
	cntr prometheus.Counter
}

func newInstrumentedReader(r io.Reader, cntr prometheus.Counter) *instrumentedReader {
	return &instrumentedReader{
		Reader: r,
		cntr:   cntr,
	}
}

func (r *instrumentedReader) Read(b []byte) (int, error) {
	n, err := r.Reader.Read(b)
	if err == nil {
		r.cntr.Add(float64(n))
	}
	return n, err
}

package prober

import "math"

type stats struct {
	peak float64
	rms  float64
}

const noSignal = -120

// Convert a float64 sample value to dB (0 is peak amplitude).
func toDB(value float64) float64 {
	if value < 1e-6 {
		return noSignal
	}
	return 20 * math.Log10(value)
}

// Analyze a buffer of samples and return some statistics.
func analyze(buf []float64) stats {
	var peak float64
	var sumSq float64
	for _, f := range buf {
		f = math.Abs(f)
		if f > peak {
			peak = f
		}
		sumSq += f * f
	}

	var s stats
	s.peak = toDB(peak)
	if len(buf) > 0 {
		s.rms = toDB(math.Sqrt(sumSq / float64(len(buf))))
	}

	return s
}

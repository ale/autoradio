package presence

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"net"
	"strings"

	pb "git.autistici.org/ale/autoradio/proto"
	"google.golang.org/protobuf/proto"
	"go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
)

// An EndpointRegistration specifies parameters for a presence
// endpoint registration. Addresses without ports will use the
// DefaultPort.
type EndpointRegistration struct {
	Prefix string
	Addrs  []net.IP
	Port   int
}

// NewRegistration creates a new EndpointRegistration.
func NewRegistration(prefix string, addrs []net.IP, port int) EndpointRegistration {
	return EndpointRegistration{
		Prefix: prefix,
		Addrs:  addrs,
		Port:   port,
	}
}

// Register one or more endpoints. The entries will be kept alive for
// the lifetime of the Session.
func Register(ctx context.Context, session *concurrency.Session, name string, regs ...EndpointRegistration) ([]*pb.Endpoint, error) {
	var endpoints []*pb.Endpoint
	for _, r := range regs {
		ep := pb.NewEndpointWithIPAndPort(name, r.Addrs, r.Port)
		if err := registerEndpoint(ctx, session, r.Prefix, ep); err != nil {
			return nil, err
		}
		endpoints = append(endpoints, ep)
	}
	return endpoints, nil
}

// RegisterEndpoint creates a new entry below prefix that exists as
// long as the underlying Session is valid or the Context is canceled.
func registerEndpoint(ctx context.Context, session *concurrency.Session, prefix string, endpoint *pb.Endpoint) error {
	data, err := proto.Marshal(endpoint)
	if err != nil {
		return err
	}
	uniqueToken := getUniqueToken()
	_, err = session.Client().Put(
		ctx,
		strings.TrimSuffix(prefix, "/")+"/"+uniqueToken,
		string(data),
		clientv3.WithLease(session.Lease()),
	)
	return err
}

func getUniqueToken() string {
	var b [16]byte
	rand.Read(b[:]) // nolint
	return hex.EncodeToString(b[:])
}

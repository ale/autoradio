package presence

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"strings"
	"sync"

	"google.golang.org/protobuf/proto"
	"go.etcd.io/etcd/client/v3"

	"git.autistici.org/ale/autoradio/coordination/watcher"
	pb "git.autistici.org/ale/autoradio/proto"
)

// EndpointSet is a container of Endpoints that is synchronizable with
// the contents of a presence tree as created by Register().
//
// Note that while an Endpoint might appear multiple times in the
// presence registry (before a stale lease expires, for instance), we
// deduplicate endpoints based on their Name.
type EndpointSet struct {
	mx sync.Mutex

	// Endpoints by etcd key.
	eps map[string]*pb.Endpoint

	// Endpoints by name.
	epn map[string]*pb.Endpoint

	// List of endpoints for fast RandomEndpoint().
	epl []*pb.Endpoint
}

func (n *EndpointSet) debugString() string {
	var eps []string
	for _, ep := range n.epl {
		eps = append(eps, fmt.Sprintf("%s(%s)", ep.Name, ep.Addrs[0]))
	}
	return fmt.Sprintf("<%s>", strings.Join(eps, ","))
}

func (n *EndpointSet) rebuildIndexes() {
	epn := make(map[string]*pb.Endpoint)
	epl := make([]*pb.Endpoint, 0, len(n.eps))
	for _, ep := range n.eps {
		if _, ok := epn[ep.Name]; !ok {
			epn[ep.Name] = ep
			epl = append(epl, ep)
		}
	}
	n.epn = epn
	n.epl = epl
}

// Reset implements the watcher.Watchable interface.
func (n *EndpointSet) Reset(m map[string]string) {
	eps := make(map[string]*pb.Endpoint)
	for k, v := range m {
		var ep pb.Endpoint
		if err := proto.Unmarshal([]byte(v), &ep); err != nil {
			log.Printf("presence: error unmarshaling %s: %v", k, err)
			continue
		}
		eps[k] = &ep
	}
	n.mx.Lock()
	n.eps = eps
	n.rebuildIndexes()
	log.Printf("presence state change (reset): %s", n.debugString())
	n.mx.Unlock()
}

// Set implements the watcher.Watchable interface.
func (n *EndpointSet) Set(k, v string) {
	var ep pb.Endpoint
	if err := proto.Unmarshal([]byte(v), &ep); err != nil {
		log.Printf("presence: error unmarshaling %s: %v", k, err)
		return
	}
	n.mx.Lock()
	if n.eps == nil {
		n.eps = make(map[string]*pb.Endpoint)
	}
	n.eps[k] = &ep
	n.rebuildIndexes()
	log.Printf("presence state change: %s", n.debugString())
	n.mx.Unlock()
}

// Delete implements the watcher.Watchable interface.
func (n *EndpointSet) Delete(k string) {
	n.mx.Lock()
	defer n.mx.Unlock()
	if n.eps == nil {
		return
	}
	if _, ok := n.eps[k]; ok {
		delete(n.eps, k)
		n.rebuildIndexes()
	}
	log.Printf("presence state change: %s", n.debugString())
}

// Endpoints returns all registered endpoints.
func (n *EndpointSet) Endpoints() []*pb.Endpoint {
	n.mx.Lock()
	defer n.mx.Unlock()
	if len(n.epl) == 0 {
		return nil
	}
	return append([]*pb.Endpoint{}, n.epl...)
}

// RandomEndpoint returns a randomly selected Endpoint (or nil, if
// none are available).
func (n *EndpointSet) RandomEndpoint() *pb.Endpoint {
	n.mx.Lock()
	defer n.mx.Unlock()
	if len(n.epl) == 0 {
		return nil
	}
	return n.epl[rand.Intn(len(n.epl))]
}

// RandomEndpointExcluding returns a randomly selected Endpoint except
// for the named one (or nil, if none are available).
func (n *EndpointSet) RandomEndpointExcluding(name string) *pb.Endpoint {
	n.mx.Lock()
	defer n.mx.Unlock()
	l := len(n.epl)
	if l == 0 {
		return nil
	}
	// Find the excluded element.
	found := -1
	for i := 0; i < l; i++ {
		if n.epl[i].Name == name {
			found = i
			break
		}
	}
	// Is the excluded element not in the list?
	if found < 0 {
		return n.epl[rand.Intn(l)]
	}
	// Is the list empty once we exclude the item?
	if l == 1 {
		return nil
	}
	i := rand.Intn(l - 1)
	if i >= found {
		i++
	}
	return n.epl[i]
}

// GetEndpoint returns a specific endpoint by host name.
func (n *EndpointSet) GetEndpoint(name string) (*pb.Endpoint, bool) {
	n.mx.Lock()
	defer n.mx.Unlock()
	ep, ok := n.epn[name]
	return ep, ok
}

func WatchEndpoints(ctx context.Context, cli *clientv3.Client, prefix string) *EndpointSet {
	var endpoints EndpointSet
	go watcher.Watch(ctx, cli, prefix, &endpoints)
	return &endpoints
}

func WatchEndpointsReady(ctx context.Context, cli *clientv3.Client, prefix string) (*EndpointSet, <-chan struct{}) {
	var endpoints EndpointSet
	w := watcher.NewReadyWatchable(&endpoints)
	go watcher.Watch(ctx, cli, prefix, w)
	return &endpoints, w.WaitForInit()
}

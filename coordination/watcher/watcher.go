package watcher

import (
	"context"
	"log"
	"sync"
	"time"

	"go.etcd.io/etcd/client/v3"
)

var watcherErrDelay = 200 * time.Millisecond

// A Watchable exposes an interface to allow synchronization with the
// remote etcd contents.
type Watchable interface {
	Reset(map[string]string)
	Set(string, string)
	Delete(string)
}

// A ReadyWatchable is a Watchable that exposes a method to let you
// know when it has the initial complete view of the data.
type ReadyWatchable interface {
	Watchable
	WaitForInit() <-chan struct{}
}

// Implementation of the above.
type readyChWatchable struct {
	Watchable

	ready   sync.Once
	readyCh chan struct{}
}

// Reset the contents of the wrapped Watchable. It will trigger the
// WaitForInit channel the first time it is called.
func (r *readyChWatchable) Reset(m map[string]string) {
	r.Watchable.Reset(m)
	r.ready.Do(func() {
		close(r.readyCh)
	})
}

func (r *readyChWatchable) WaitForInit() <-chan struct{} {
	return r.readyCh
}

// NewReadyWatchable wraps a Watchable with a WaitForInit method.
func NewReadyWatchable(w Watchable) ReadyWatchable {
	return &readyChWatchable{
		Watchable: w,
		readyCh:   make(chan struct{}),
	}
}

// A NotifyWatchable is a Watchable with a channel to receive updates
// whenever the data changes.
type NotifyWatchable interface {
	Watchable
	Notify() <-chan struct{}
}

// Implementation of the above.
type notifyChWatchable struct {
	Watchable
	notifyCh chan struct{}
}

func (n *notifyChWatchable) Notify() <-chan struct{} {
	return n.notifyCh
}

func (n *notifyChWatchable) awaken() {
	select {
	case n.notifyCh <- struct{}{}:
	default:
	}
}

func (n *notifyChWatchable) Reset(m map[string]string) {
	n.Watchable.Reset(m)
	n.awaken()
}

func (n *notifyChWatchable) Set(k, v string) {
	n.Watchable.Set(k, v)
	n.awaken()
}

func (n *notifyChWatchable) Delete(k string) {
	n.Watchable.Delete(k)
	n.awaken()
}

// NewNotifyWatchable returns a NotifyWatchable wrapping another
// Watchable.
func NewNotifyWatchable(ctx context.Context, w Watchable) NotifyWatchable {
	notifyCh := make(chan struct{}, 1)
	// Close the channel when the context is done.
	go func() {
		<-ctx.Done()
		close(notifyCh)
	}()
	return &notifyChWatchable{
		Watchable: w,
		notifyCh:  notifyCh,
	}
}

func watchOnce(ctx context.Context, cli *clientv3.Client, prefix string, target Watchable) error {
	plen := len(prefix)

	// First populate the target with a prefix-ranged Get and call
	// Reset because we have all the authoritative data.
	resp, err := cli.Get(ctx, prefix, clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortAscend))
	if err != nil {
		return err
	}
	m := make(map[string]string)
	for _, ev := range resp.Kvs {
		key := string(ev.Key)[plen:]
		m[key] = string(ev.Value)
	}
	target.Reset(m)
	rev := resp.Header.Revision

	// Then start the Watcher on the same revision.
	rch := cli.Watch(ctx, prefix, clientv3.WithPrefix(), clientv3.WithRev(rev))
	for resp := range rch {
		for _, ev := range resp.Events {
			key := string(ev.Kv.Key)[plen:]
			switch ev.Type {
			case clientv3.EventTypePut:
				target.Set(key, string(ev.Kv.Value))
			case clientv3.EventTypeDelete:
				target.Delete(key)
			}
		}
	}

	// If we get here, the Watcher has been somehow disrupted, but
	// we don't have a specific error to return.
	return nil
}

// Watch a prefix on etcd and synchronize its contents with
// target. The function will return when the context is canceled,
// otherwise it will continuously attempt to re-establish the
// underlying Watcher on error.
func Watch(ctx context.Context, cli *clientv3.Client, prefix string, target Watchable) {
	for {
		err := watchOnce(ctx, cli, prefix, target)
		if err == context.Canceled {
			return
		} else if err != nil {
			log.Printf("watcher error: %s: %v", prefix, err)
		}

		// Wait a bit, then retry.
		time.Sleep(watcherErrDelay)
	}
}

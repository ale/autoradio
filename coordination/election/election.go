package election

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	pb "git.autistici.org/ale/autoradio/proto"
	"go.etcd.io/etcd/client/v3/concurrency"
	"google.golang.org/protobuf/proto"
)

// Op is a function that will be called when a node participating in
// an election becomes the leader. Leadership is resigned once the
// function exits (or is canceled via the Context).
type Op func(context.Context) error

// WaitForever is an Op that does nothing (until the Context is canceled).
func WaitForever(ctx context.Context) error {
	<-ctx.Done()
	return ctx.Err()
}

// Possible values for ElectionState.State.
const (
	StateUnknown = iota
	StateFollower
	StateLeader
)

// ElectionState represents the current state of the election, with
// the value of the current leader endpoint, if any.
type ElectionState struct {
	State  int
	Leader *pb.Endpoint
}

func (s ElectionState) String() string {
	switch s.State {
	case StateFollower:
		return fmt.Sprintf("follower(%s)", s.Leader.String())
	case StateLeader:
		return "leader"
	default:
		return "unknown"
	}
}

// Election manages (or witnesses) an election protocol on a given
// database prefix.
type Election struct {
	path     string
	session  *concurrency.Session
	election *concurrency.Election
	self     *pb.Endpoint
}

// New creates a new Election, scoped within the provided session. If
// self is not nil, the caller participates in the election and can
// call Run().
func New(session *concurrency.Session, path string, self *pb.Endpoint) *Election {
	el := concurrency.NewElection(session, path)
	return &Election{
		path:     path,
		session:  session,
		election: el,
		self:     self,
	}
}

// Watch the election and get notified of state changes.
func (e *Election) Watch(ctx context.Context) <-chan ElectionState {
	ch := make(chan ElectionState, 10)
	go func() {
		defer close(ch)
		for resp := range e.election.Observe(ctx) {
			var state ElectionState
			var ep pb.Endpoint
			if err := proto.Unmarshal(resp.Kvs[0].Value, &ep); err != nil {
				continue
			}
			state.Leader = &ep
			// A non-participant will have self == nil.
			if e.self != nil && proto.Equal(&ep, e.self) {
				state.State = StateLeader
			} else {
				state.State = StateFollower
			}
			select {
			case ch <- state:
			default:
			}
		}
	}()
	return ch
}

// Run the election as a participant, and run op when we're the leader.
func (e *Election) Run(ctx context.Context, op Op) error {
	data, err := proto.Marshal(e.self)
	if err != nil {
		return err
	}
	for {
		err := e.runOnce(ctx, string(data), op)
		if errors.Is(err, context.Canceled) {
			return err
		} else if err != nil {
			log.Printf("election: %v", err)
		}
	}
}

func (e *Election) runOnce(ctx context.Context, data string, op Op) error {
	// Attempt to acquire the leader lock.
	err := e.election.Campaign(ctx, data)
	if err != nil {
		return err
	}

	// Invoke the leader operation.
	err = op(ctx)

	// Resign, if the session is still valid. Use a standalone
	// Context to resign even on cancellation.
	rctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	rerr := e.election.Resign(rctx)
	cancel()
	if rerr != nil {
		log.Printf("election: error when resigning: %v", rerr)
	}

	return err
}

// An ElectionWatcher simply consumes the output of Election.Watch and
// stores the most recent result so you can query it asynchronously.
type ElectionWatcher struct {
	mx       sync.Mutex
	curState ElectionState
	notifyCh chan struct{}
}

// NewWatcher creates a new ElectionWatcher.
func NewWatcher(ctx context.Context, el *Election) *ElectionWatcher {
	w := &ElectionWatcher{
		notifyCh: make(chan struct{}, 1),
	}
	go func() {
		defer close(w.notifyCh)
		for state := range el.Watch(ctx) {
			w.mx.Lock()
			w.curState = state
			w.mx.Unlock()
			select {
			case w.notifyCh <- struct{}{}:
			default:
			}
		}
	}()
	return w
}

// State returns the most recent ElectionState.
func (w *ElectionWatcher) State() ElectionState {
	w.mx.Lock()
	defer w.mx.Unlock()
	return w.curState
}

// Notify returns a channel that is triggered on every state change.
func (w *ElectionWatcher) Notify() <-chan struct{} {
	return w.notifyCh
}

// Observe an election without participating in it. It's a simple way
// to track the current leader.
func Observe(ctx context.Context, session *concurrency.Session, path string) <-chan ElectionState {
	return New(session, path, nil).Watch(ctx)
}

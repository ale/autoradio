package proto

import (
	"errors"
	"fmt"
	"strings"
)

// IsRelay returns true if the stream is configured as a relay of an
// external source.
func (m *Mount) IsRelay() bool {
	return m.RelayUrl != ""
}

// HasTranscoder returns true if the stream has transcoding sub-streams.
func (m *Mount) HasTranscoder() bool {
	return m.Transcode
}

// Valid performs a consistency check and returns true if the
// configuration for the stream is correct.
func (m *Mount) Valid() error {
	if !strings.HasPrefix(m.Path, "/") {
		return errors.New("path does not start with a slash")
	}
	if m.SourceUsername != "" && m.SourcePassword == "" {
		return errors.New("username is set but password is empty")
	}
	if m.SourceUsername == "" && m.SourcePassword != "" {
		return errors.New("password is set but username is empty")
	}
	if m.RelayUrl != "" && m.Transcode {
		return errors.New("relay_url and transcode can't both be set")
	}
	if m.TranscodeParams != nil {
		if err := m.TranscodeParams.Valid(); err != nil {
			return fmt.Errorf("invalid encoding parameters: %v", err)
		}
	}
	return nil
}

// Valid returns true if the EncodingParams seem to make sense. We try
// to be as close to the liquidsoap capabilities as possible.
func (p *EncodingParams) Valid() error {
	switch p.Format {
	case "mp3", "mp3.cbr", "mp3.abr", "vorbis.cbr", "vorbis.abr":
		if p.BitRate == 0 {
			return errors.New("bitrate not specified")
		}
	case "mp3.vbr":
		if p.Quality < 0 || p.Quality > 9 {
			return errors.New("quality must be in range [0, 9]")
		}
	case "vorbis":
		if p.Quality < -0.2 || p.Quality > 1 {
			return errors.New("quality must be in range [-0.2, 1]")
		}
	case "":
		return errors.New("format not specified")
	default:
		return fmt.Errorf("unknown format \"%s\"", p.Format)
	}
	if p.SampleRate == 0 {
		return errors.New("sample rate not specified")
	}
	if p.Channels < 1 || p.Channels > 2 {
		return errors.New("bad number of channels")
	}
	if p.Channels > 1 {
		switch p.StereoMode {
		case "", "stereo", "joint_stereo", "default":
		default:
			return fmt.Errorf("unknown stereo mode \"%s\"", p.StereoMode)
		}
	}
	return nil
}

func (p *EncodingParams) DebugString() string {
	var out []string
	out = append(out, p.Format)
	if p.BitRate > 0 {
		out = append(out, fmt.Sprintf("%dkBps", p.BitRate))
	}
	if p.Quality > -1 {
		out = append(out, fmt.Sprintf("q=%g", p.Quality))
	}
	switch p.Channels {
	case 1:
		out = append(out, "mono")
	case 2:
		out = append(out, "stereo")
	}
	if p.SampleRate > 0 {
		out = append(out, fmt.Sprintf("%gkHz", float64(p.SampleRate)/1000))
	}
	return strings.Join(out, ", ")
}

package proto

import (
	"net"
	"strconv"
)

// NewEndpointWithIPAndPort creates an Endpoint with the specified IP
// address and port.
func NewEndpointWithIPAndPort(name string, ips []net.IP, port int) *Endpoint {
	ep := Endpoint{
		Name: name,
	}
	sport := strconv.Itoa(port)
	for _, ip := range ips {
		ep.Addrs = append(ep.Addrs, net.JoinHostPort(ip.String(), sport))
	}
	return &ep
}

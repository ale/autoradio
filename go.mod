module git.autistici.org/ale/autoradio

go 1.14

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/go-kit/kit v0.10.0 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/google/subcommands v1.2.0
	github.com/jmcvetta/randutil v0.0.0-20150817122601-2bb1b664bcff
	github.com/kevinpollet/nego v0.0.0-20201213172553-d6ce2e30cfd6 // indirect
	github.com/lpar/gzipped/v2 v2.0.2
	github.com/miekg/dns v1.1.45
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.32.1
	go.etcd.io/etcd/client/v3 v3.5.1
	go.etcd.io/etcd/server/v3 v3.5.1
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/sync v0.4.0
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
)

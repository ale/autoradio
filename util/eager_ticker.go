package util

import (
	"context"
	"time"
)

// RunCron invokes fn periodically, until the context is canceled. It
// is a simple wrapper for time.Ticker, but fn is also called
// immediately as soon as the timer starts.
func RunCron(ctx context.Context, d time.Duration, fn func(context.Context)) {
	// Call fn right away, unless the Context is already invalid.
	select {
	case <-ctx.Done():
		return
	default:
	}
	fn(ctx)

	tick := time.NewTicker(d)
	defer tick.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-tick.C:
			fn(ctx)
		}
	}
}

package util

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
)

// WriteFileIfChanged updates the contents of a file if they have
// changed. It returns false if the file already exists and its
// contents are equal to 'data', true in any other case.
func WriteFileIfChanged(path string, data []byte) (bool, error) {
	if cur, err := ioutil.ReadFile(path); err == nil && bytes.Equal(cur, data) {
		return false, nil
	}

	tmpf, err := ioutil.TempFile(filepath.Dir(path), ".tmp")
	if err != nil {
		return true, err
	}
	defer os.Remove(tmpf.Name())

	tmpf.Write(data)
	tmpf.Close()
	os.Chmod(tmpf.Name(), 0644)

	return true, os.Rename(tmpf.Name(), path)
}

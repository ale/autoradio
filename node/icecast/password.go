package icecast

import (
	"io/ioutil"
	"os"
	"strings"

	"git.autistici.org/ale/autoradio"
)

// Return the Icecast admin password saved in 'adminPwPath'. If it
// does not exist, generate a random one and save it for subsequent
// invocations.
func getAdminPassword(adminPwPath string) (string, error) {
	data, err := ioutil.ReadFile(adminPwPath)
	if err == nil {
		return strings.TrimSpace(string(data)), nil
	}

	if !os.IsNotExist(err) {
		return "", err
	}
	pw := autoradio.GeneratePassword()
	if err := ioutil.WriteFile(adminPwPath, append([]byte(pw), '\n'), 0600); err != nil {
		return "", err
	}
	return pw, nil
}

package icecast

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"git.autistici.org/ale/autoradio"
	pb "git.autistici.org/ale/autoradio/proto"
)

var (
	icecastStatusPage           = "/status-json.xsl"
	icecastStatusUpdateInterval = 2 * time.Second
)

// The JSON status document returned by Icecast does not really have a
// schema: the XSL adaptor will generate "plausible" JSON by guessing
// the data types. This means that if someone sets the current song
// title to, say, "1234", this will be encoded in the resulting JSON
// as a number, not a string, and the naive encoding/json
// deserialization to string will fail with a type error.
//
// Since this data collection is optimistic anyway, we create our own
// "tolerant string" type and eat those errors away. This is used on
// all fields that are ultimately user-controlled.
type maybeString string

func (s *maybeString) UnmarshalJSON(data []byte) error {
	var tmp string
	if err := json.Unmarshal(data, &tmp); err == nil {
		*s = maybeString(tmp)
	}
	return nil
}

// TODO: deserialize properly the time format used by Icecast.
type icecastMountStatus struct {
	Artist      maybeString `json:"artist"`
	BitRate     int32       `json:"audio_bitrate"`
	Channels    int32       `json:"audio_channels"`
	AudioInfo   string      `json:"audio_info"`
	SampleRate  int32       `json:"audio_samplerate"`
	Genre       maybeString `json:"genre"`
	Listeners   int32       `json:"listeners"`
	ListenURL   string      `json:"listenurl"`
	Quality     string      `json:"quality"`
	Description maybeString `json:"server_description"`
	Name        maybeString `json:"server_name"`
	Type        string      `json:"server_type"`
	Subtype     string      `json:"subtype"`
	Title       maybeString `json:"title"`
	//StreamStart time.Time `json:"stream_start_iso8601"`
}

// Icecast status-json.xsl returns different structures if there is a
// single source or more than one.
type icecastStatusManySources struct {
	Icestats struct {
		Source []icecastMountStatus `json:"source"`
	} `json:"icestats"`
}

type icecastStatusSingleSource struct {
	Icestats struct {
		Source icecastMountStatus `json:"source"`
	} `json:"icestats"`
}

func parseIcecastStatus(r io.Reader) ([]icecastMountStatus, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	// Try the single source document schema first.
	var single icecastStatusSingleSource
	if err := json.Unmarshal(data, &single); err == nil {
		return []icecastMountStatus{single.Icestats.Source}, nil
	}
	var many icecastStatusManySources
	if err := json.Unmarshal(data, &many); err != nil {
		return nil, err
	}
	return many.Icestats.Source, nil
}

func fetchIcecastStatus(ctx context.Context, port int) ([]icecastMountStatus, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:%d%s", port, icecastStatusPage), nil)
	if err != nil {
		return nil, err
	}
	resp, err := httpClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
	return parseIcecastStatus(resp.Body)
}

func convertIcecastStatus(status []icecastMountStatus) []*pb.IcecastMount {
	out := make([]*pb.IcecastMount, 0, len(status))
	for _, m := range status {
		listenURL, err := url.Parse(m.ListenURL)
		if err != nil {
			continue
		}
		outm := pb.IcecastMount{
			Path:        autoradio.IcecastPathToMountPath(listenURL.Path),
			Listeners:   m.Listeners,
			BitRate:     m.BitRate,
			SampleRate:  m.SampleRate,
			Channels:    m.Channels,
			Name:        string(m.Name),
			Description: string(m.Description),
			Title:       string(m.Title),
			Artist:      string(m.Artist),
		}
		// For whatever reason, quality is a string in Icecast's JSON.
		if q, err := strconv.ParseFloat(m.Quality, 32); err == nil {
			outm.Quality = float32(q)
		}
		out = append(out, &outm)
	}
	return out
}

func (c *Controller) statusUpdater(ctx context.Context) {
	tick := time.NewTicker(icecastStatusUpdateInterval)
	defer tick.Stop()
	for {
		select {
		case <-tick.C:
			uctx, cancel := context.WithTimeout(ctx, icecastHTTPTimeout)
			status, err := fetchIcecastStatus(uctx, autoradio.IcecastPort)
			cancel()

			c.statusMx.Lock()
			if err != nil {
				log.Printf("icecast_status: error: %v", err)
				c.icecastOk = false
				c.mounts = nil
			} else {
				c.icecastOk = true
				c.mounts = convertIcecastStatus(status)
			}
			c.statusMx.Unlock()

		case <-ctx.Done():
			return
		}
	}
}

func (c *Controller) GetStatus() ([]*pb.IcecastMount, bool) {
	c.statusMx.Lock()
	defer c.statusMx.Unlock()
	return c.mounts, c.icecastOk
}

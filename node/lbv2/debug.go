package lbv2

import (
	"fmt"
	"html/template"
	"net/http"
)

const debugText = `<html>
    <body>
    <title>Load Balancer</title>

    <h3>Query costs</h3>
    <table>
      <tr><th>Node</th><th>Requests</th><th>Utilization/Cost</th></tr>
      {{$dimensions := .Dimensions}}
      {{range .Nodes}}
      <tr>
        <td>{{.Name}}</td>
        <td>{{.Requests}}</td>
        {{$data := .Data}}
        {{range $d := $dimensions}}
          {{$cur := index $data $d}}
          <td>
          {{$cur.PredictedUtilization}}/{{$cur.ReportedUtilization}}/{{$cur.Cost}}
          </td>
        {{end}}
      </tr>
      {{end}}
    </table>
    </body>
    </html>`

var debugTmpl = template.Must(template.New("lbv2 debug").Parse(debugText))

type nodeDebugData struct {
	Name     string
	Requests int
	Data     map[int]costAndUtil
}

type costAndUtil struct {
	ReportedUtilization  float64
	PredictedUtilization float64
	Cost                 float64
}

func (l *LoadBalancer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Build a view of the cost/utilization data.
	l.lock.Lock()
	var nodes []nodeDebugData
	for i := 0; i < l.nodes.Len(); i++ {
		n := l.nodes.Get(i)
		ndata := nodeDebugData{Name: n.Name(), Data: make(map[int]costAndUtil)}
		for dim, pred := range l.predictors {
			util := n.Utilization(dim)
			ndata.Requests = util.Requests
			ndata.Data[dim] = costAndUtil{
				ReportedUtilization:  util.Utilization,
				PredictedUtilization: pred.Utilization(n),
				Cost:                 float64(pred.cost[n.Name()]),
			}
		}
		nodes = append(nodes, ndata)
	}
	var dimensions []int
	for dim := range l.predictors {
		dimensions = append(dimensions, dim)
	}
	l.lock.Unlock()

	ctx := struct {
		Nodes         []nodeDebugData
		Dimensions    []int
		NumDimensions int
	}{nodes, dimensions, len(dimensions)}

	err := debugTmpl.Execute(w, ctx)
	if err != nil {
		fmt.Fprintln(w, "debug: error executing template: ", err.Error())
	}
}

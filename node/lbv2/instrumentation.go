package lbv2

import (
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestsDesc = prometheus.NewDesc(
		"lbv2_requests",
		"Number of requests sent to each upstream.",
		[]string{"upstream", "dimension"}, nil,
	)
	reportedUtilDesc = prometheus.NewDesc(
		"lbv2_reported_utilization",
		"Utilization (reported).",
		[]string{"upstream", "dimension"}, nil,
	)
	predictedUtilDesc = prometheus.NewDesc(
		"lbv2_predicted_utilization",
		"Utilization (predicted).",
		[]string{"upstream", "dimension"}, nil,
	)
	costDesc = prometheus.NewDesc(
		"lbv2_cost",
		"Estimated query cost.",
		[]string{"upstream", "dimension"}, nil,
	)
)

// Generating lb metrics is a relatively expensive operation, so we
// only do it at scraping time by implementing our own custom
// prometheus.Collector.
type loadBalancerCollector struct {
	*LoadBalancer
}

func (lc loadBalancerCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- requestsDesc
	ch <- reportedUtilDesc
	ch <- predictedUtilDesc
	ch <- costDesc
}

type nodeMetrics struct {
	name      string
	dimension int
	requests  int
	util      float64
	pred      float64
	cost      float64
}

func (lc loadBalancerCollector) Collect(ch chan<- prometheus.Metric) {
	lc.LoadBalancer.lock.Lock()
	data := make([]nodeMetrics, 0, lc.LoadBalancer.nodes.Len())
	for i := 0; i < lc.LoadBalancer.nodes.Len(); i++ {
		n := lc.LoadBalancer.nodes.Get(i)
		for dim, pred := range lc.LoadBalancer.predictors {
			util := n.Utilization(dim)
			data = append(data, nodeMetrics{
				name:      n.Name(),
				dimension: dim,
				requests:  util.Requests,
				util:      util.Utilization,
				pred:      pred.Utilization(n),
				cost:      float64(pred.cost[n.Name()]),
			})
		}
	}
	lc.LoadBalancer.lock.Unlock()

	for _, d := range data {
		dimLabel := strconv.Itoa(d.dimension)
		ch <- prometheus.MustNewConstMetric(
			requestsDesc,
			prometheus.CounterValue,
			float64(d.requests),
			d.name,
			dimLabel,
		)
		ch <- prometheus.MustNewConstMetric(
			reportedUtilDesc,
			prometheus.GaugeValue,
			d.util,
			d.name,
			dimLabel,
		)
		ch <- prometheus.MustNewConstMetric(
			predictedUtilDesc,
			prometheus.GaugeValue,
			d.pred,
			d.name,
			dimLabel,
		)
		ch <- prometheus.MustNewConstMetric(
			costDesc,
			prometheus.GaugeValue,
			d.cost,
			d.name,
			dimLabel,
		)
	}
}

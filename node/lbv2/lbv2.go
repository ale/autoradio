// Modular building blocks for a traffic control engine.
//
package lbv2

import (
	"errors"
	"log"
	"math/rand"
	"net"
	"sync"
	"time"

	"github.com/jmcvetta/randutil"
	"github.com/prometheus/client_golang/prometheus"
)

// Node utilization along a specific dimension. Utilization is treated
// as a vector, the LoadBalancer is opaque to the actual meaning of
// the dimensions used (though it makes sense for Requests to be the
// same across dimensions).
type NodeUtilization struct {
	// Utilization is a number between 0 and 1.
	Utilization float64

	// Some request-related metric. It doesn't really matter
	// whether it is a counter or a gauge, as long as it is
	// roughly proportional to the utilization.
	Requests int
}

type Node interface {
	// Name of the node (unique identifier).
	Name() string

	// Current utilization for the specified dimension.
	Utilization(int) NodeUtilization
}

type NodeScore struct {
	Score float64
	Node  Node
}

// SetScore applies a multiplier to the current score and returns
// the modified object.
func (c NodeScore) SetScore(w float64) NodeScore {
	c.Score *= w
	return c
}

// Damping factor for the utilization query cost model.
const costAlpha = 0.8

type costEstimate float64

func (e costEstimate) Update(util NodeUtilization) costEstimate {
	// Compute the new query cost and update the current cost
	// (with a damping factor).
	newCost := util.Utilization / float64(1+util.Requests)
	return costEstimate(costAlpha*float64(e) + (1-costAlpha)*newCost)
}

// A Predictor estimates utilization along a specific dimension using
// a (relatively simple) cost query model.
type Predictor interface {
	Utilization(Node) float64
}

type costPredictor struct {
	cost          map[string]costEstimate
	util          map[string]float64
	utilDimension int
	clusterSize   float64
}

func newPredictor(utilDimension int) *costPredictor {
	return &costPredictor{
		clusterSize:   1,
		utilDimension: utilDimension,
		cost:          make(map[string]costEstimate),
		util:          make(map[string]float64),
	}
}

// Update the cost function based on the most recent node utilization.
func (p *costPredictor) Update(nodes NodeList) {
	p.clusterSize = float64(nodes.Len())
	for i := 0; i < nodes.Len(); i++ {
		// Update the current query cost estimate. If the node
		// is new, set the cost to 1 which results in a
		// gradual ramp-up of connections as the damping
		// function subsides.
		n := nodes.Get(i)
		name := n.Name()
		util := n.Utilization(p.utilDimension)
		if prev, ok := p.cost[name]; ok {
			p.cost[name] = prev.Update(util)
		} else {
			p.cost[name] = costEstimate(1)
		}

		// Update the predicted utilization.
		p.util[name] = util.Utilization
	}
}

func (p *costPredictor) Incr(n Node) {
	// The zeroth-order approximation is that every redirector
	// sees the same data and follows the same query distribution.
	p.util[n.Name()] += float64(p.cost[n.Name()]) * p.clusterSize
}

func (p *costPredictor) Utilization(n Node) float64 {
	return p.util[n.Name()]
}

// Provides contextual information on the incoming request.
type RequestContext interface {
	RemoteAddr() net.IP
}

// NodeList is a wrapper for a container of Nodes.
type NodeList interface {
	Len() int
	Get(int) Node
}

// The LoadBalancer object makes decisions about where traffic should
// go. It contains the list of active backend nodes (updated
// asynchronously by calling Update), and a list of filters and
// policies used to select a backend for every incoming request.
//
// Filters look at each available backend node and assign them a
// score, possibly depending on the incoming request. The policy then
// picks a backend among the available nodes based on their score. By
// convention, it is possible to remove a node from the list of
// candidates by setting its score to zero.
//
// Combinations of simple filters can implement relatively complex
// traffic control behaviors: from straightforward round-robin to
// geoip latency minimization to capacity-aware, utilization-based
// load balancing.
//
// The LoadBalancer keeps track of how many requests were sent to each
// backend node, to compute an up-to-date estimation of its current
// utilization. This should help mitigate "thundering herd" and "laser
// death ray" scenarios caused by delays in the utilization feedback
// loop.
//
// The computational model makes some generic assumptions about
// incoming traffic, the results will be better the more real traffic
// actually matches these assumptions. First, all requests are assumed
// to be identical, utilization-wise: there is a single query cost
// metric. While this implies that the accuracy of the cost estimation
// for each specific query may be low, the effect evens out with large
// numbers as long as the statistical distribution of request types
// varies little over time. Secondly, since the estimation logic is
// local to each frontend, the model assumes that each frontend
// receives an equal share of incoming traffic.
//
type LoadBalancer struct {
	lock       sync.Mutex
	nodes      NodeList
	predictors map[int]*costPredictor
	filters    []NodeFilter
	policy     Policy

	// Only use for testing purposes.
	disablePredictors bool
}

// New returns a new LoadBalancer with no filters or policy set. The
// node name is only used to set a label on the associated Prometheus
// collector (in case multiple LoadBalancers are created within the
// same process).
func New(name string) *LoadBalancer {
	lb := &LoadBalancer{
		predictors: make(map[int]*costPredictor),
	}

	lc := loadBalancerCollector{lb}
	// TODO: we're currently using (due to etcd) an older
	// Prometheus client, which does not have WrapRegistererWith.
	// prometheus.WrapRegistererWith(prometheus.Labels{"name":
	// name}, prometheus.DefaultRegisterer).MustRegister(lc)
	if err := prometheus.Register(lc); err != nil {
		if _, ok := err.(prometheus.AlreadyRegisteredError); !ok {
			panic(err)
		}
	}

	return lb
}

// AddFilters appends a filter to the filter list.
func (l *LoadBalancer) AddFilter(f NodeFilter) {
	l.filters = append(l.filters, f)
}

// SetPolicy sets the node selection policy.
func (l *LoadBalancer) SetPolicy(p Policy) {
	l.policy = p
}

// GetPredictor returns an utilization predictor for the specified
// dimension.
func (l *LoadBalancer) GetPredictor(dimension int) Predictor {
	p, ok := l.predictors[dimension]
	if !ok {
		p = newPredictor(dimension)
		l.predictors[dimension] = p
	}
	return p
}

// Only use for testing purposes.
func (l *LoadBalancer) DisableUtilizationPredictors() {
	l.disablePredictors = true
}

// Update the set of known nodes. The new utilization numbers will be
// used to calibrate the utilization predictors.
func (l *LoadBalancer) Update(nodes NodeList) {
	l.lock.Lock()
	defer l.lock.Unlock()
	l.nodes = nodes
	for _, p := range l.predictors {
		p.Update(nodes)
	}
}

// Possible error values returned by Choose.
var (
	ErrNoNodes          = errors.New("no nodes are known")
	ErrAllNodesFiltered = errors.New("all nodes were filtered")
	ErrPolicy           = errors.New("nodes rejected by policy")
)

// Choose a node according to the specified policies.
func (l *LoadBalancer) Choose(ctx RequestContext) (Node, error) {
	l.lock.Lock()
	defer l.lock.Unlock()
	if l.nodes == nil || l.nodes.Len() == 0 {
		return nil, ErrNoNodes
	}

	// Create the candidate list.
	wnodes := make([]NodeScore, 0, l.nodes.Len())
	for i := 0; i < l.nodes.Len(); i++ {
		wnodes = append(wnodes, NodeScore{
			Score: 1.0,
			Node:  l.nodes.Get(i),
		})
	}

	// Apply filters.
	for _, f := range l.filters {
		wnodes = f.Filter(ctx, wnodes)
	}
	if len(wnodes) == 0 {
		return nil, ErrAllNodesFiltered
	}

	// Select a node among the available candidates.
	n := l.policy.GetNode(wnodes)
	if n == nil {
		return nil, ErrPolicy
	}

	// Feed back the choice into the utilization predictors.
	if !l.disablePredictors {
		for _, p := range l.predictors {
			p.Incr(n)
		}
	}

	return n, nil
}

type Policy interface {
	GetNode([]NodeScore) Node
}

type PolicyFunc func([]NodeScore) Node

func (f PolicyFunc) GetNode(wnodes []NodeScore) Node {
	return f(wnodes)
}

// Node filters operate on the list of available nodes (and associated
// weights), possibly modifying it in-place. They can be composed out
// of simpler parts, like individual nodeScorers wrapped by an
// nodeScorerFilter (when no request-specific initialization step is
// required).
type NodeFilter interface {
	Filter(RequestContext, []NodeScore) []NodeScore
}

// Score a single node.
type NodeScorer interface {
	Score(RequestContext, Node) float64
}

type NodeScorerFunc func(RequestContext, Node) float64

func (f NodeScorerFunc) Score(ctx RequestContext, n Node) float64 {
	return f(ctx, n)
}

// Run an individual NodeScorer over all available nodes. Satisfies
// the NodeFilter interface.
type nodeScorerFilterWrapper struct {
	NodeScorer
}

func (f *nodeScorerFilterWrapper) Filter(ctx RequestContext, wnodes []NodeScore) []NodeScore {
	for i, wn := range wnodes {
		wnodes[i] = wnodes[i].SetScore(f.NodeScorer.Score(ctx, wn.Node))
	}
	return wnodes
}

func NodeScorerFilter(s NodeScorer) NodeFilter {
	return &nodeScorerFilterWrapper{s}
}

// Give each node a score of 1.0 - utilization.
type capacityAvailableScorer struct {
	pred Predictor
}

func (s *capacityAvailableScorer) Score(ctx RequestContext, n Node) float64 {
	u := s.pred.Utilization(n)
	return 1.0 - u
}

func NewCapacityAvailableScorer(pred Predictor) NodeFilter {
	return NodeScorerFilter(&capacityAvailableScorer{pred})
}

// Disable nodes that have no available capacity (utilization greater
// than 1).
type capacityAvailableFilter struct {
	pred Predictor
}

func (f *capacityAvailableFilter) Score(ctx RequestContext, n Node) float64 {
	if f.pred.Utilization(n) >= 1 {
		return 0
	}
	return 1
}

func NewCapacityAvailableFilter(pred Predictor) NodeFilter {
	return NodeScorerFilter(&capacityAvailableFilter{pred})
}

// Remove disabled nodes (weight=0) from the list of candidates.
type activeNodesFilter struct{}

func (f *activeNodesFilter) Filter(ctx RequestContext, wnodes []NodeScore) []NodeScore {
	// Remove from the list of candidates the nodes whose score is
	// equal to 0. Modifies the list in-place moving non-zero
	// elements to the top of the list.
	o := 0
	for i, n := range wnodes {
		if n.Score > 0 {
			if i != o {
				wnodes[o] = wnodes[i]
			}
			o++
		}
	}
	return wnodes[:o]
}

func NewActiveNodesFilter() NodeFilter {
	return &activeNodesFilter{}
}

const baseWeight = 1000000

// Return a random item based on a weighted distribution.
func weightedPolicyFunc(wnodes []NodeScore) Node {
	// Need to convert this anyway. We keep track of the maximum
	// weight becaue if they are all zeros randutil.WeightedChoice
	// will return an error. We fall back to random choice in that
	// case.
	choices := make([]randutil.Choice, len(wnodes))
	var maxWeight int
	for i, wn := range wnodes {
		w := int(float64(baseWeight) * wn.Score)
		if w > maxWeight {
			maxWeight = w
		}
		choices[i] = randutil.Choice{
			Weight: w,
			Item:   wn.Node,
		}
	}

	if maxWeight == 0 {
		return randomPolicyFunc(wnodes)
	}

	result, err := randutil.WeightedChoice(choices)
	if err != nil {
		// We log these in order to detect randutil edge cases.
		log.Printf("lbv2: randutil error: %v", err)
		return nil
	}
	return result.Item.(Node)
}

var WeightedPolicy = PolicyFunc(weightedPolicyFunc)

// Return a random item regardless of their score.
func randomPolicyFunc(wnodes []NodeScore) Node {
	if len(wnodes) == 0 {
		return nil
	}
	return wnodes[rand.Intn(len(wnodes))].Node
}

var RandomPolicy = PolicyFunc(randomPolicyFunc)

// Always return the item with the highest score (only good for
// testing purposes, really). Falls back to random selection in case
// of multiple nodes with the same score.
func highestScorePolicyFunc(wnodes []NodeScore) Node {
	var maxScore float64
	best := make([]int, 0, len(wnodes))
	for i, n := range wnodes {
		switch {
		case n.Score > maxScore:
			maxScore = n.Score
			best = best[:0]
			best = append(best, i)
		case n.Score == maxScore:
			best = append(best, i)
		}
	}
	switch len(best) {
	case 0:
		return nil
	case 1:
		return wnodes[best[0]].Node
	default:
		return wnodes[best[rand.Intn(len(best))]].Node
	}
}

var HighestScorePolicy = PolicyFunc(highestScorePolicyFunc)

func init() {
	// Seed the math/rand PRNG. The current time works just fine,
	// it doesn't need to be cryptographically robust.
	rand.Seed(time.Now().Unix())
}

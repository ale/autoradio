package node

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
)

var (
	prometheusURL     = flag.String("prometheus-url", "", "Prometheus server API URL")
	prometheusTimeout = flag.Duration("prometheus-timeout", 30*time.Second, "Timeout for Prometheus requests")

	knownQueries = map[string]string{
		"stream_listeners": "sum(stream_listeners{stream=~\"$arg\"}) by (stream)",
		"all_listeners":    "global:stream_listeners:sum",
	}
)

// The metricsProxy passes through some predefined queries to a
// Prometheus instance (that is monitoring autoradio), and returns
// timeseries data in a Javascript-friendly format. We don't want to
// expose Prometheus directly.
type metricsProxy struct {
	client api.Client
}

func newMetricsProxy() (*metricsProxy, error) {
	client, err := api.NewClient(api.Config{
		Address: *prometheusURL,
	})
	if err != nil {
		return nil, err
	}
	return &metricsProxy{
		client: client,
	}, nil
}

func (p *metricsProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	query, rng, err := p.queryFromRequest(r)
	if err != nil {
		log.Printf("bad metrics request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	v1api := v1.NewAPI(p.client)
	ctx, cancel := context.WithTimeout(r.Context(), *prometheusTimeout)
	defer cancel()

	result, _, err := v1api.QueryRange(ctx, query, rng)
	if err != nil {
		log.Printf("error querying Prometheus: query='%s': %v", query, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data, labels, err := tablify(result)
	if err != nil {
		log.Printf("error parsing Prometheus data: query='%s': %v", query, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	// nolint: errcheck
	json.NewEncoder(w).Encode(map[string]interface{}{
		"data":   data,
		"labels": labels,
	})
}

func (p *metricsProxy) queryFromRequest(r *http.Request) (string, v1.Range, error) {
	name := r.FormValue("query")
	d, err := time.ParseDuration(r.FormValue("t"))
	if err != nil {
		d = time.Hour
	}

	// We only care that the argument does not contain quotes.
	arg := r.FormValue("arg")
	if strings.Contains(arg, "\"") {
		return "", v1.Range{}, errors.New("bad argument")
	}
	q, ok := knownQueries[name]
	if !ok {
		return "", v1.Range{}, errors.New("unknown query")
	}
	q = strings.Replace(q, "$arg", arg, -1)

	rng := v1.Range{
		Start: time.Now().Add(-d),
		End:   time.Now(),
		Step:  time.Minute,
	}

	return q, rng, nil
}

// tablify converts a QueryRange result into an array of float64
// arrays that can be directly fed to uPlot via javascript.
func tablify(result model.Value) ([][]float64, []string, error) {
	m := result.(model.Matrix)

	if len(m) == 0 {
		return nil, nil, errors.New("no values")
	}

	table := make([][]float64, len(m)+1)
	// Avoid n^2 allocations.
	for i := 0; i < len(table); i++ {
		table[i] = make([]float64, 0, len(m[0].Values))
	}
	var labels []string
	for idx, row := range m {
		labels = append(labels, row.Metric.String())
		for _, val := range row.Values {
			// Row 0 is the timestamp.
			if idx == 0 {
				table[0] = append(table[0], float64(val.Timestamp))
			}
			table[idx+1] = append(table[idx+1], float64(val.Value))
		}
	}

	return table, labels, nil
}

package node

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"sort"
	"strconv"

	pb "git.autistici.org/ale/autoradio/proto"
)

var tpl *template.Template

func init() {
	tpl = mustParseEmbeddedTemplates()
}

// Parse the templates that are embedded with the binary (in bindata.go).
func mustParseEmbeddedTemplates() *template.Template {
	root := template.New("")
	files, err := AssetDir("templates")
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		b, err := Asset("templates/" + f)
		if err != nil {
			log.Fatalf("could not read embedded template %s: %v", f, err)
		}
		if _, err := root.New(f).Parse(string(b)); err != nil {
			log.Fatalf("error parsing template %s: %v", f, err)
		}
	}
	return root
}

// mountStatus reports the configuration and status of a mount,
// including eventual transcoded mounts that source it.
type mountStatus struct {
	Mount        *pb.Mount
	IcecastMount *pb.IcecastMount
	Listeners    int
	TransMounts  []*mountStatus
}

func (m *mountStatus) totalListeners() int {
	l := m.Listeners
	for _, ms := range m.TransMounts {
		l += ms.Listeners
	}
	return l
}

func newMountStatus(m *pb.Mount, nodes []*nodeInfo, icecastMounts map[string]*pb.IcecastMount) *mountStatus {
	var listeners int
	for _, n := range nodes {
		for _, ims := range n.status.IcecastMounts {
			if ims.Path == m.Path {
				listeners += int(ims.Listeners)
				break
			}
		}
	}
	return &mountStatus{
		Mount:        m,
		Listeners:    listeners,
		IcecastMount: icecastMounts[m.Path],
	}
}

type statusList []*pb.Status

func (l statusList) Len() int      { return len(l) }
func (l statusList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l statusList) Less(i, j int) bool {
	return l[i].Name < l[j].Name
}

type mountStatusList []*mountStatus

func (l mountStatusList) Len() int      { return len(l) }
func (l mountStatusList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l mountStatusList) Less(i, j int) bool {
	return l[i].Mount.Path < l[j].Mount.Path
}

// mountsToStatus converts a list of mounts (and eventually the
// current list of nodes) to a nicely sorted and tree-aggregated list
// of mountStatus objects. The list of nodes can be nil, in which case
// listener statistics will be omitted.
func mountsToStatus(mounts []*pb.Mount, nodes []*nodeInfo, icecastMounts map[string]*pb.IcecastMount) []*mountStatus {
	// Aggregate stats, and create a tree of transcoding mounts.
	// We only recurse twice because we don't allow transcodes of
	// transcoded streams.
	ms := make(map[string]*mountStatus)
	for _, m := range mounts {
		if m.HasTranscoder() {
			continue
		}
		ms[m.Path] = newMountStatus(m, nodes, icecastMounts)
	}
	for _, m := range mounts {
		if !m.HasTranscoder() {
			continue
		}
		src := ms[m.TranscodeParams.SourcePath]
		if src == nil {
			continue
		}
		src.TransMounts = append(src.TransMounts, newMountStatus(m, nodes, icecastMounts))
	}
	msl := make([]*mountStatus, 0, len(ms))
	for _, m := range ms {
		msl = append(msl, m)
	}

	// Sort everything (including transcoded mounts).
	sort.Sort(mountStatusList(msl))
	for _, s := range msl {
		if s.TransMounts != nil {
			sort.Sort(mountStatusList(s.TransMounts))
		}
	}
	return msl
}

func filterActiveMountStatus(mounts []*mountStatus) []*mountStatus {
	out := make([]*mountStatus, 0, len(mounts))
	for _, ms := range mounts {
		if ms.totalListeners() > 0 {
			out = append(out, ms)
		}
	}
	return out
}

type statusPageHandler struct {
	n      *Node
	domain string
}

func newStatusPageHandler(n *Node, domain string) *statusPageHandler {
	return &statusPageHandler{n: n, domain: domain}
}

func (s *statusPageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Convert the list of nodes to just the status. While we're
	// at it, build a map of mount path -> exemplary IcecastMount,
	// which we use to show the current song artist / title.
	nodes := s.n.lb.getNodes()
	statuses := make([]*pb.Status, 0, len(nodes))
	exemplary := make(map[string]*pb.IcecastMount)
	for _, ni := range nodes {
		statuses = append(statuses, ni.status)
		for _, im := range ni.status.IcecastMounts {
			if _, ok := exemplary[im.Path]; !ok {
				exemplary[im.Path] = im
			}
		}
	}
	sort.Sort(statusList(statuses))

	ms := filterActiveMountStatus(
		mountsToStatus(s.n.mounts.GetMounts(), nodes, exemplary))
	vars := struct {
		Domain string
		Nodes  []*pb.Status
		Mounts []*mountStatus
	}{s.domain, statuses, ms}

	var buf bytes.Buffer
	if err := tpl.ExecuteTemplate(&buf, "index.html", vars); err != nil {
		log.Printf("error rendering status page: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	addDefaultHeaders(w)
	w.Write(buf.Bytes()) //nolint
}

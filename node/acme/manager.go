package acme

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	mrand "math/rand"
	"strings"
	"sync"
	"time"

	"go.etcd.io/etcd/client/v3"
)

const (
	certPath = "acme/cert"
	keyPath  = "acme/key"
)

var (
	checkIntervalSeconds = 9600
	renewalTimeout       = 1800 * time.Second
	renewalDays          = 22
)

type Cert struct {
	Priv []byte
	Pub  [][]byte
}

func (c *Cert) Names() (names []string) {
	if cert, err := x509.ParseCertificate(c.Pub[0]); err == nil {
		for _, dn := range cert.DNSNames {
			names = append(names, strings.TrimPrefix(dn, "DNS:"))
		}
	}
	return
}

func (c *Cert) TLSCertificate() (*tls.Certificate, error) {
	pkey, err := parsePrivateKey(c.Priv)
	if err != nil {
		return nil, err
	}
	return &tls.Certificate{
		Certificate: c.Pub,
		PrivateKey:  pkey,
	}, nil
}

func (c *Cert) NotAfter() time.Time {
	cert, err := x509.ParseCertificate(c.Pub[0])
	if err != nil {
		return time.Time{}
	}
	return cert.NotAfter
}

// A Manager is responsible for a single SSL certificate (which may
// have multiple names). It will store the certificate itself, and the
// ACME state, on etcd, so that it is replicated to all HTTPS servers.
//
// Renewal is handled via (internal) cron jobs, with random schedules
// to avoid having to implement leader-election for such a simple task.
//
type Manager struct {
	*ACME

	names []string
	cli   *clientv3.Client

	// Keep the Certificate and the parsed version in sync.
	mx              sync.RWMutex
	cert            *Cert
	tlsCert         *tls.Certificate
	renewalDeadline time.Time
}

func NewManager(ctx context.Context, cli *clientv3.Client, email, directoryURL string, certNames []string) (*Manager, error) {
	// Instantiate the ACME protocol handler and have it store its
	// validation tokens on etcd.
	acmeMgr := NewACME(email, directoryURL, newEtcdKeyStore(cli, keyPath), newEtcdTokenStore(cli))

	m := &Manager{
		ACME:  acmeMgr,
		names: certNames,
		cli:   cli,
	}

	// Try to fetch the existing certificate from etcd, or
	// generate a self-signed one. fetchCert can independently
	// return a nil certificate or a nil error.
	cert, rev, err := fetchCert(ctx, cli, certPath)
	if err != nil {
		log.Printf("acme: error fetching certificate: %v", err)
	}
	if cert == nil {
		cert, err = makeSelfSignedCert(certNames)
		if err != nil {
			return nil, fmt.Errorf("failed to create self-signed certificate: %v", err)
		}
	}

	if err := m.setCert(cert); err != nil {
		return nil, err
	}

	// Update m.cert using a watcher.
	go m.watch(ctx, certPath, rev)

	// Start the background renewer job.
	go m.renewLoop(ctx)

	return m, nil
}

func (m *Manager) GetCertificate(_ *tls.ClientHelloInfo) (*tls.Certificate, error) {
	m.mx.RLock()
	defer m.mx.RUnlock()
	return m.tlsCert, nil
}

func (m *Manager) setCert(cert *Cert) error {
	tlsCert, err := cert.TLSCertificate()
	if err != nil {
		return err
	}

	m.mx.Lock()
	m.cert = cert
	m.tlsCert = tlsCert
	m.renewalDeadline = cert.NotAfter().AddDate(0, 0, -renewalDays)
	log.Printf("acme: found certificate (renewal deadline: %s)", m.renewalDeadline.Format(time.Stamp))
	m.mx.Unlock()

	return nil
}

func (m *Manager) shouldRenew() (bool, string) {
	m.mx.RLock()
	defer m.mx.RUnlock()
	if time.Now().After(m.renewalDeadline) {
		return true, fmt.Sprintf("met renewal deadline %s", m.renewalDeadline.Format(time.Stamp))
	} else if names := m.cert.Names(); !listsEqual(names, m.names) {
		return true, fmt.Sprintf("name list changed (actual: %v, desired: %v)", names, m.names)
	}
	return false, ""
}

func (m *Manager) renewLoop(ctx context.Context) {
	// Initial delay to stagger concurrent initialization.
	time.Sleep(time.Duration(mrand.Intn(300)) * time.Second)

	for {
		if renew, reason := m.shouldRenew(); renew {
			log.Printf("acme: attempting to renew SSL certificate: %s", reason)
			if err := m.renew(ctx); err != nil {
				log.Printf("acme: certificate renewal failed: %v", err)
			} else {
				log.Printf("acme: successfully renewed SSL certificate")
			}
		}

		// Sleep a semi-random amount of time.
		t := time.After(time.Duration(checkIntervalSeconds/2+mrand.Intn(checkIntervalSeconds)) * time.Second)
		select {
		case <-ctx.Done():
			return
		case <-t:
		}
	}
}

func (m *Manager) renew(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, renewalTimeout)
	defer cancel()

	m.mx.RLock()
	cert := m.cert
	m.mx.RUnlock()

	var key crypto.Signer
	var err error

	if len(cert.Priv) == 0 {
		key, err = rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return err
		}
		cert.Priv, err = x509.MarshalPKCS8PrivateKey(key)
	} else {
		key, err = parsePrivateKey(cert.Priv)
	}
	if err != nil {
		return err
	}

	cert.Pub, _, err = m.ACME.getCertificate(ctx, key, m.names)
	if err != nil {
		return err
	}

	return storeCert(ctx, m.cli, certPath, cert)
}

func (m *Manager) watchOnce(ctx context.Context, path string, rev int64) error {
	for resp := range m.cli.Watch(ctx, path, clientv3.WithRev(rev)) {
		for _, ev := range resp.Events {
			if ev.Type != clientv3.EventTypePut {
				continue
			}
			var cert Cert
			if err := json.Unmarshal(ev.Kv.Value, &cert); err != nil {
				log.Printf("acme: error unmarshaling cert: %v", err)
				continue
			}

			if err := m.setCert(&cert); err != nil {
				log.Printf("acme: error reading saved cert: %v", err)
			}
		}
	}

	// Watcher is gone, recover.
	return nil
}

func (m *Manager) watch(ctx context.Context, path string, rev int64) {
	for {
		err := m.watchOnce(ctx, path, rev)
		if err == context.Canceled {
			return
		} else if err != nil {
			log.Printf("acme: watcher error: %s: %v", path, err)
		}

		time.Sleep(watcherErrDelay)

		cert, newRev, err := fetchCert(ctx, m.cli, path)
		if err != nil {
			log.Printf("acme: fetch error: %s: %v", path, err)
		} else if cert != nil {
			if err := m.setCert(cert); err != nil {
				log.Printf("acme: error reading saved cert: %v", err)
			}
			rev = newRev
		}
	}
}

func makeSelfSignedCert(names []string) (*Cert, error) {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	keyBytes, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return nil, fmt.Errorf("marshaling error: %v", err)
	}

	notBefore := time.Now().AddDate(0, 0, -1)
	notAfter := notBefore.AddDate(1, 0, 0)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %v", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Autoradio"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		DNSNames:              names,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		return nil, err
	}

	return &Cert{
		Pub:  [][]byte{derBytes},
		Priv: keyBytes,
	}, nil
}

func listsEqual(a, b []string) bool {
	tmp := make(map[string]struct{})
	for _, aa := range a {
		tmp[aa] = struct{}{}
	}
	for _, bb := range b {
		if _, ok := tmp[bb]; !ok {
			return false
		}
		delete(tmp, bb)
	}
	return len(tmp) == 0
}

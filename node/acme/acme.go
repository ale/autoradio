package acme

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"golang.org/x/crypto/acme"
)

const http01Challenge = "http-01"

var acmeRPCTimeout = 30 * time.Second

type keyBackend interface {
	Put(context.Context, []byte) error
	Get(context.Context) ([]byte, error)
}

type tokenBackend interface {
	Put(context.Context, string, string) error
	Get(context.Context, string) (string, error)
	Delete(context.Context, string)
}

// The ACME object implements the ACME protocol machinery, and can be
// used to create and renew certificates. It also serves as an HTTP
// handler to satisfy http-01 validation requests.
type ACME struct {
	email        string
	directoryURL string

	mx       sync.Mutex
	client   *acme.Client
	keystore keyBackend
	tokens   tokenBackend
}

func NewACME(email, directoryURL string, keystore keyBackend, tokens tokenBackend) *ACME {
	if directoryURL == "" {
		directoryURL = acme.LetsEncryptURL
	}
	return &ACME{
		email:        email,
		directoryURL: directoryURL,
		keystore:     keystore,
		tokens:       tokens,
	}
}

// Load the account key from the state file and return it, possibly
// initializing it if it does not exist.
func (a *ACME) accountKey(ctx context.Context) (crypto.Signer, error) {
	var keyData []byte
	var err error

	// Run a get/put loop a few times to resolve eventual
	// conflicts when servers have synchronized startups.
	for i := 0; i < 10; i++ {
		cctx, cancel := context.WithTimeout(ctx, acmeRPCTimeout)
		keyData, err = a.keystore.Get(cctx)
		cancel()

		if err != nil {
			log.Printf("acme: generating new account key")
			eckey, kerr := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			if kerr != nil {
				return nil, kerr
			}
			keyData, kerr = x509.MarshalECPrivateKey(eckey)
			if kerr != nil {
				return nil, kerr
			}

			cctx, cancel = context.WithTimeout(ctx, acmeRPCTimeout)
			err = a.keystore.Put(cctx, keyData)
			cancel()
		}

		if err == nil {
			break
		}
	}
	if err != nil {
		return nil, err
	}

	return parsePrivateKey(keyData)
}

// Return a new acme.Client, possibly initializing the account key if
// it does not exist yet. The Client is created on the first call and
// cached thereafter.
func (a *ACME) acmeClient(ctx context.Context) (*acme.Client, error) {
	a.mx.Lock()
	defer a.mx.Unlock()

	if a.client != nil {
		return a.client, nil
	}

	client := &acme.Client{
		DirectoryURL: a.directoryURL,
	}
	key, err := a.accountKey(ctx)
	if err != nil {
		return nil, err
	}
	client.Key = key
	ac := &acme.Account{
		Contact: []string{"mailto:" + a.email},
	}

	// Register the account (accept TOS) if necessary. If the
	// account is already registered we get a StatusConflict,
	// which we can ignore.
	_, err = client.Register(ctx, ac, func(_ string) bool { return true })
	if ae, ok := err.(*acme.Error); err == nil || err == acme.ErrAccountAlreadyExists || (ok && ae.StatusCode == http.StatusConflict) {
		a.client = client
		err = nil
	}
	return a.client, err
}

// fulfill the validation request by storing the token.
func (a *ACME) fulfill(ctx context.Context, client *acme.Client, _ string, chal *acme.Challenge) (func(), error) {
	resp, err := client.HTTP01ChallengeResponse(chal.Token)
	if err != nil {
		return nil, err
	}
	path := client.HTTP01ChallengePath(chal.Token)

	if err := a.tokens.Put(ctx, path, resp); err != nil {
		return nil, err
	}

	return func() {
		go func() {
			a.tokens.Delete(ctx, path)
		}()
	}, nil
}

// getCertificate returns a certificate chain (and the parsed leaf
// cert), given a private key and a list of domains. The first domain
// will be the subject CN, the others will be added as subjectAltNames.
func (a *ACME) getCertificate(ctx context.Context, key crypto.Signer, names []string) (der [][]byte, leaf *x509.Certificate, err error) {
	client, err := a.acmeClient(ctx)
	if err != nil {
		return nil, nil, err
	}

	o, err := a.verifyAll(ctx, client, names)
	if err != nil {
		return nil, nil, err
	}

	csr, err := certRequest(key, names)
	if err != nil {
		return nil, nil, err
	}
	der, _, err = client.CreateOrderCert(ctx, o.FinalizeURL, csr, true)
	if err != nil {
		return nil, nil, err
	}
	leaf, err = validCert(names, der, key)
	if err != nil {
		return nil, nil, err
	}
	return der, leaf, nil
}

func (a *ACME) verifyAll(ctx context.Context, client *acme.Client, names []string) (*acme.Order, error) {
	// Make an authorization request to the ACME server, and
	// verify that it returns a valid response with challenges.
	o, err := client.AuthorizeOrder(ctx, acme.DomainIDs(names...))
	if err != nil {
		return nil, fmt.Errorf("AuthorizeOrder failed: %v", err)
	}

	switch o.Status {
	case acme.StatusReady:
		return o, nil // already authorized
	case acme.StatusPending:
	default:
		return nil, fmt.Errorf("invalid new order status %q", o.Status)
	}

	for _, zurl := range o.AuthzURLs {
		z, err := client.GetAuthorization(ctx, zurl)
		if err != nil {
			return nil, fmt.Errorf("GetAuthorization(%s) failed: %v", zurl, err)
		}
		if z.Status != acme.StatusPending {
			continue
		}
		// Pick a challenge that matches our preferences and the
		// available validators. The validator fulfills the challenge,
		// and returns a cleanup function that we're going to call
		// before we return. All steps are sequential and idempotent.
		chal := pickChallenge(z.Challenges)
		if chal == nil {
			return nil, fmt.Errorf("unable to authorize %q", names)
		}

		// We only support http-01 challenges.
		if chal.Type != http01Challenge {
			return nil, fmt.Errorf("challenge type '%s' is not available", chal.Type)
		}

		for _, domain := range names {
			cleanup, err := a.fulfill(ctx, client, domain, chal)
			if err != nil {
				return nil, fmt.Errorf("fulfillment failed: %v", err)
			}
			defer cleanup()
		}

		if _, err := client.Accept(ctx, chal); err != nil {
			return nil, fmt.Errorf("challenge accept failed: %v", err)
		}
		if _, err := client.WaitAuthorization(ctx, z.URI); err != nil {
			return nil, fmt.Errorf("WaitAuthorization(%s) failed: %v", z.URI, err)
		}
	}

	// Authorizations are satisfied, wait for the CA
	// to update the order status.
	if _, err = client.WaitOrder(ctx, o.URI); err != nil {
		return nil, err
	}
	return o, nil
}

func (a *ACME) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	token, err := a.tokens.Get(req.Context(), req.URL.Path)
	if err != nil {
		http.NotFound(w, req)
		return
	}
	io.WriteString(w, token) // nolint: errcheck
}

// Pick a challenge with the right type from the Challenge response
// returned by the ACME server. We only support http-01, so we just
// search for that one.
func pickChallenge(chal []*acme.Challenge) *acme.Challenge {
	for _, ch := range chal {
		if ch.Type == http01Challenge {
			return ch
		}
	}
	return nil
}

func certRequest(key crypto.Signer, domains []string) ([]byte, error) {
	req := &x509.CertificateRequest{
		Subject: pkix.Name{CommonName: domains[0]},
	}
	if len(domains) > 1 {
		req.DNSNames = domains[1:]
	}
	return x509.CreateCertificateRequest(rand.Reader, req, key)
}

func parsePrivateKey(der []byte) (crypto.Signer, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey:
			return key, nil
		case *ecdsa.PrivateKey:
			return key, nil
		default:
			return nil, errors.New("unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}

	return nil, errors.New("failed to parse private key")
}

package acme

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"go.etcd.io/etcd/client/v3"
)

var (
	watcherErrDelay = 1 * time.Second
	certOpTimeout   = 10 * time.Second
)

func fetchCert(ctx context.Context, cli *clientv3.Client, path string) (*Cert, int64, error) {
	// Create a context with a timeout.
	cctx, cancel := context.WithTimeout(ctx, certOpTimeout)
	defer cancel()

	// Run a Get first.
	resp, err := cli.Get(cctx, path)
	if err != nil {
		return nil, 0, err
	}
	rev := resp.Header.Revision
	if len(resp.Kvs) == 0 {
		return nil, rev, nil
	}
	var cert Cert
	if err := json.Unmarshal(resp.Kvs[0].Value, &cert); err != nil {
		return nil, rev, err
	}
	return &cert, rev, nil
}

func storeCert(ctx context.Context, cli *clientv3.Client, path string, cert *Cert) error {
	// Create a context with a timeout.
	cctx, cancel := context.WithTimeout(ctx, certOpTimeout)
	defer cancel()

	data, err := json.Marshal(cert)
	if err != nil {
		return err
	}

	_, err = cli.Put(cctx, path, string(data))
	return err
}

const tokenPrefix = "acme/tokens"

type etcdTokenStore struct {
	cli *clientv3.Client
}

func newEtcdTokenStore(cli *clientv3.Client) *etcdTokenStore {
	return &etcdTokenStore{cli: cli}
}

func (t *etcdTokenStore) Put(ctx context.Context, key, value string) error {
	_, err := t.cli.Put(ctx, tokenPrefix+key, value)
	return err
}

func (t *etcdTokenStore) Get(ctx context.Context, key string) (string, error) {
	resp, err := t.cli.Get(ctx, tokenPrefix+key)
	if err != nil {
		return "", err
	}
	if len(resp.Kvs) == 0 {
		return "", errors.New("not found")
	}
	return string(resp.Kvs[0].Value), nil
}

func (t *etcdTokenStore) Delete(ctx context.Context, key string) {
	t.cli.Delete(ctx, tokenPrefix+key) // nolint: errcheck
}

type etcdKeyStore struct {
	cli  *clientv3.Client
	path string
}

func newEtcdKeyStore(cli *clientv3.Client, path string) *etcdKeyStore {
	return &etcdKeyStore{cli: cli, path: path}
}

func (k *etcdKeyStore) Put(ctx context.Context, value []byte) error {
	kvc := clientv3.NewKV(k.cli)

	_, err := kvc.Txn(ctx).
		If(clientv3.CreateRevision(k.path)).
		Then(clientv3.OpPut(k.path, string(value))).
		Commit()
	return err
}

func (k *etcdKeyStore) Get(ctx context.Context) ([]byte, error) {
	resp, err := k.cli.Get(ctx, k.path)
	if err != nil {
		return nil, err
	}
	if len(resp.Kvs) == 0 {
		return nil, errors.New("not found")
	}
	return resp.Kvs[0].Value, nil
}

package acme

import "testing"

func TestMakeSelfSignedCert(t *testing.T) {
	cert, err := makeSelfSignedCert([]string{"name1", "name2"})
	if err != nil {
		t.Fatal(err)
	}
	if len(cert.Pub) == 0 {
		t.Fatal("empty cert")
	}
}

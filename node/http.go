package node

//go:generate make
//go:generate go-bindata --nocompress --pkg node static/... templates/...

import (
	"bytes"
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/node/acme"
	pb "git.autistici.org/ale/autoradio/proto"
	"github.com/NYTimes/gziphandler"
	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/lpar/gzipped/v2"
)

var (
	disableDebugHandlers  = flag.Bool("http-disable-debug", false, "disable HTTP /debug handlers")
	restrictDebugHandlers = flag.Bool("http-restrict-debug", false, "restrict access to /debug from localhost only")
)

// A simple AssetFS wrapper that provides the Exists() method needed
// by the gzipped.FileSystem interface.
type assetFSWrapper struct {
	*assetfs.AssetFS
}

func (f *assetFSWrapper) Exists(name string) bool {
	_, err := f.AssetFS.Asset(name)
	return err == nil
}

func newAssetFSWrapper() *assetFSWrapper {
	return &assetFSWrapper{
		AssetFS: &assetfs.AssetFS{
			Asset:     Asset,
			AssetDir:  AssetDir,
			AssetInfo: AssetInfo,
			Prefix:    "static",
		},
	}
}

// Build the HTTP handler for the public HTTP endpoint.
func newHTTPHandler(n *Node, icecastPort int, domain string) http.Handler {
	mux := http.NewServeMux()

	// Serve /static/ from builtin assets. Also serve directly
	// /favicon.ico using the same mechanism.
	fs := withCachingHeaders(gzipped.FileServer(newAssetFSWrapper()))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))
	mux.Handle("/favicon.ico", fs)

	// Simple healthcheck handler.
	mux.HandleFunc("/healthcheck", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		io.WriteString(w, "OK") //nolint
	})

	// Serve /debug/ pages using the default HTTP handler
	// (packages will automatically register their debug handlers
	// there). Using command-line flags it is possible to disable
	// the default debug pages, or to restrict access to localhost.
	if !*disableDebugHandlers {
		mux.Handle("/debug/lbv2", n.lb.lb)
		var h http.Handler = http.DefaultServeMux
		if *restrictDebugHandlers {
			h = withLocalhost(h)
		}
		mux.Handle("/debug/", h)
	}

	// Metrics API (Prometheus proxy).
	if *prometheusURL != "" {
		mp, err := newMetricsProxy()
		if err != nil {
			log.Printf("warning: metrics proxy disabled due to error: %v", err)
		} else {
			mux.Handle("/timeseries_query", gziphandler.GzipHandler(mp))
		}
	}

	// A very narrow selection of Icecast API methods is served
	// below /admin/ and forwarded to the Icecast master via a
	// standard httputil.ReverseProxy.
	adminHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		leaderAddr := n.leaderAddr()
		if leaderAddr == "" {
			http.Error(w, "No leader", http.StatusServiceUnavailable)
			return
		}

		rp := httputil.NewSingleHostReverseProxy(&url.URL{
			Scheme: "http",
			Host:   leaderAddr,
		})
		rp.ServeHTTP(w, r)
	})
	mux.Handle("/admin/metadata", adminHandler)
	mux.Handle("/admin/killsource", adminHandler)

	// Requests for /_stream/ go straight to the local Icecast.
	proxyHandler := http.StripPrefix(autoradio.IcecastMountPrefix,
		withMount(n, func(m *pb.Mount, w http.ResponseWriter, r *http.Request) {
			doIcecastProxy(w, r, &url.URL{
				Scheme: "http",
				Host:   fmt.Sprintf("127.0.0.1:%d", icecastPort),
				Path:   autoradio.MountPathToIcecastPath(m.Path),
			}, m.Path, false)
		}))

	// redirectHandler serves different kinds of redirects, either
	// an M3U or a HTTP redirect, for the public stream URLs (ones
	// without the /_stream prefix).
	redirectHandler := withMount(n, func(m *pb.Mount, w http.ResponseWriter, r *http.Request) {
		serveRedirect(n.lb, m, w, r)
	})

	// sourceHandler deals with SOURCE requests to the public
	// stream URL, which are forwarded straight to the master
	// Icecast node.
	sourceHandler := withMount(n, func(m *pb.Mount, w http.ResponseWriter, r *http.Request) {
		serveSource(n, m, w, r)
	})

	// statusHandler serves the home status page.
	statusHandler := gziphandler.GzipHandler(newStatusPageHandler(n, domain))

	// playerHandler serves the HTML audio player.
	playerHandler := gziphandler.GzipHandler(withMount(n, func(m *pb.Mount, w http.ResponseWriter, r *http.Request) {
		servePlayer(m, w, r, domain)
	}))
	mux.Handle("/player/", http.StripPrefix("/player", playerHandler))

	streamPrefixSlash := autoradio.IcecastMountPrefix + "/"
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.Method == "GET" && strings.HasPrefix(r.URL.Path, streamPrefixSlash):
			proxyHandler.ServeHTTP(w, r)
		case r.Method == "SOURCE" || r.Method == "PUT":
			sourceHandler.ServeHTTP(w, r)
		case r.Method == "GET" && (r.URL.Path == "" || r.URL.Path == "/"):
			statusHandler.ServeHTTP(w, r)
		case r.Method == "GET":
			redirectHandler.ServeHTTP(w, r)
		default:
			http.NotFound(w, r)
		}
	})

	return mux
}

// Request wrapper that passes a Mount along with the HTTP
// request. The mount path is just the URL path without an eventual
// .m3u extension.
func withMount(n *Node, f func(*pb.Mount, http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mountPath := strings.TrimSuffix(r.URL.Path, ".m3u")
		mount, ok := n.mounts.GetMount(mountPath)
		if !ok {
			//log.Printf("http: %s: mount point not found", mountPath)
			http.NotFound(w, r)
			return
		}
		f(mount, w, r)
	})
}

func serveSource(n *Node, mount *pb.Mount, w http.ResponseWriter, r *http.Request) {
	// Can't connect sources to relay streams.
	if mount.IsRelay() {
		log.Printf("error: source connection to relay stream %s", mount.Path)
		http.Error(w, "Stream is relayed, no source connections allowed", http.StatusBadRequest)
		return
	}

	targetAddr := n.leaderAddr()
	if targetAddr == "" {
		http.Error(w, "No leader", http.StatusServiceUnavailable)
		return
	}

	log.Printf("connecting source for %s to %s", mount.Path, targetAddr)
	doIcecastProxy(w, r, &url.URL{
		Scheme: "http",
		Host:   targetAddr,
		Path:   autoradio.MountPathToIcecastPath(mount.Path),
	}, mount.Path, true)
}

func serveRedirect(lb *loadBalancer, mount *pb.Mount, w http.ResponseWriter, r *http.Request) {
	// The M3U file just sends to the plain redirect endpoint (as
	// people might save the .m3u file).
	if strings.HasSuffix(r.URL.Path, ".m3u") {
		sendM3U(w, r)
		return
	}

	// Redirect the user to a final target, depending on the load
	// balancing algorithm's decision.  This enforces the
	// 1:1 mapping between Icecasts and frontends.
	targetNode := lb.chooseNode(&httpRequestContext{r})
	if targetNode == nil {
		log.Printf("error: http: %s: no nodes available", mount.Path)
		http.Error(w, "No nodes available", http.StatusServiceUnavailable)
		return
	}

	// Use the node hostname for the redirect (compatible with using SSL certs).
	targetURL := url.URL{
		Scheme: schemeFromRequest(r),
		Host:   targetNode.ep.Name,
		Path:   autoradio.MountPathToIcecastPath(mount.Path),
	}

	sendRedirect(w, r, targetURL.String())
}

func servePlayer(m *pb.Mount, w http.ResponseWriter, r *http.Request, domain string) {
	// Build the stream URL using the incoming request.
	streamURL := url.URL{
		Scheme: schemeFromRequest(r),
		Host:   r.Host,
		Path:   m.Path,
	}

	// Make up the audio MIME type from the stream path.
	mimeType := "audio/" + strings.TrimPrefix(filepath.Ext(m.Path), ".")

	vars := struct {
		Domain string
		Name   string
		Type   string
		URL    string
	}{domain, m.Path, mimeType, streamURL.String()}

	var buf bytes.Buffer
	if err := tpl.ExecuteTemplate(&buf, "player.html", vars); err != nil {
		log.Printf("error rendering player page: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	addDefaultHeaders(w)
	w.Write(buf.Bytes()) //nolint
}

// Serve a M3U response. This simply points back at the stream
// redirect handler by dropping the .m3u suffix in the request URL.
func sendM3U(w http.ResponseWriter, r *http.Request) {
	// Build a fully qualified URL using the Host header from the incoming request.
	m3url := url.URL{
		Scheme: schemeFromRequest(r),
		Host:   r.Host,
		Path:   strings.TrimSuffix(r.URL.Path, ".m3u"),
	}
	m3us := m3url.String()

	w.Header().Set("Content-Length", strconv.Itoa(len(m3us)))
	w.Header().Set("Content-Type", "audio/x-mpegurl")
	addDefaultHeaders(w)
	io.WriteString(w, m3us) //nolint
}

func sendRedirect(w http.ResponseWriter, r *http.Request, targetURL string) {
	// Firefox apparently caches redirects regardless of
	// the status code, so we have to add some quite
	// aggressive cache-busting headers. We serve a status
	// code of 307 to HTTP/1.1 clients, 302 otherwise.
	w.Header().Set("Cache-Control", "max-age=0,no-cache,no-store")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "-1")
	code := http.StatusFound
	if r.ProtoMinor == 1 {
		code = http.StatusTemporaryRedirect
	}
	http.Redirect(w, r, targetURL, code)
}

func addDefaultHeaders(w http.ResponseWriter) {
	w.Header().Set("Expires", "-1")
	w.Header().Set("Cache-Control", "no-cache,no-store")
}

func schemeFromRequest(r *http.Request) string {
	if r.TLS != nil {
		return "https"
	}
	return "http"
}

// Restrict access to localhost.
func withLocalhost(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		host, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		if ip := net.ParseIP(host); !ip.IsLoopback() {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// Add cache-friendly headers.
func withCachingHeaders(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "max-age=31536000")
		w.Header().Set("Expires", time.Now().AddDate(1, 0, 0).Format(http.TimeFormat))
		h.ServeHTTP(w, r)
	})
}

// Wrapper to make an http.Server match the genericServer interface.
type httpServer struct {
	*http.Server
	name string
}

func newHTTPServer(name, addr string, h http.Handler) *httpServer {
	return &httpServer{
		Server: &http.Server{
			Addr:              addr,
			Handler:           h,
			ReadTimeout:       10 * time.Second,
			ReadHeaderTimeout: 3 * time.Second,
			//WriteTimeout:      10 * time.Second,
			IdleTimeout: 30 * time.Second,
		},
		name: name,
	}
}

func (s *httpServer) Name() string { return s.name }

func (s *httpServer) Start(ctx context.Context) error {
	go func() {
		<-ctx.Done()

		// Create an standalone context with a short timeout.
		sctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		s.Server.Shutdown(sctx) // nolint
		cancel()
	}()
	err := s.Server.ListenAndServe()
	if err == http.ErrServerClosed {
		err = nil
	}
	return err
}

type httpsServer struct {
	*httpServer
}

func newHTTPSServer(name, addr string, h http.Handler, mgr *acme.Manager) *httpsServer {
	s := &httpsServer{
		httpServer: newHTTPServer(name, addr, h),
	}
	s.httpServer.Server.TLSConfig = &tls.Config{
		SessionTicketsDisabled:   true,
		PreferServerCipherSuites: true,
		MinVersion:               tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
		},
		GetCertificate: mgr.GetCertificate,
	}

	return s
}

func (s *httpsServer) Start(ctx context.Context) error {
	go func() {
		<-ctx.Done()

		// Create an standalone context with a short timeout.
		sctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		s.Server.Shutdown(sctx) // nolint
		cancel()
	}()
	err := s.Server.ListenAndServeTLS("", "")
	if err == http.ErrServerClosed {
		err = nil
	}
	return err
}

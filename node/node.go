package node

import (
	"context"
	"log"
	"net"
	"time"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/client"
	"git.autistici.org/ale/autoradio/coordination/election"
	"git.autistici.org/ale/autoradio/coordination/presence"
	pb "git.autistici.org/ale/autoradio/proto"
	"git.autistici.org/ale/autoradio/util"
	"github.com/prometheus/client_golang/prometheus"
	"go.etcd.io/etcd/client/v3/concurrency"
)

var statusUpdateInterval = 1 * time.Second

// Icecast is the interface used to manage the local Icecast daemon.
type Icecast interface {
	GetStatus() ([]*pb.IcecastMount, bool)
	Update(context.Context, []*pb.Mount, bool, string) error
}

// A Node controls an Icecast instance and participates in various
// presence/consensus protocols in order to be part of the autoradio
// cluster. Its only communications are with Icecast and the
// frontends. A Node comes with its own etcd Session, that it
// maintains until the controlling Context is canceled.
type Node struct {
	name         string
	maxBandwidth int
	maxListeners int

	mounts    client.MountConfig
	watcher   *election.ElectionWatcher
	statusMgr *statusManager
	updateCh  chan struct{}
	ice       Icecast
	lb        *loadBalancer

	ctx context.Context
}

// New returns a new Node with a controlling Context, scoped to an etcd
// Session.
func New(parentCtx context.Context, session *concurrency.Session, ice Icecast, nodeID string, publicAddrs []net.IP, peerAddr net.IP, gossipPort int, lbSpec string, maxBandwidth, maxListeners int) (*Node, error) {
	// Creating a Node starts a number of background goroutines and etcd
	// watchers. Here's how those parts interact:
	//
	// The main task of the Node is to manage the Icecast daemon by
	// ensuring its configuration is in sync with the contents of our
	// configuration (in etcd). The Node controls Icecast by generating
	// its configuration and sending it a SIGHUP to reload it.
	//
	// Changes in the Icecast configuration originate from two sources:
	// changes in the mount configuration, or state transitions of the
	// Icecast leader. So we listen on both the configuration and the
	// election notification channels, and use a third channel to send the
	// new configuration to Icecast: in both cases, the other half of the
	// configuration is cached and looked up asynchronously.
	//
	// The second part of the Node functionality has to do with
	// maintaining a view of the state of all nodes so that we can use it
	// for load balancing decisions. This is delegated to the loadBalancer
	// type.

	// Create a sub-Context that can be canceled when Stop is called. This
	// Context controls the lifetime of the Node itself, and all the
	// associated background goroutines: canceling it immediately
	// terminates all outbound requests.
	ctx, cancel := context.WithCancel(parentCtx)
	go func() {
		// Oops, the session is gone, stop everything.
		<-session.Done()
		cancel()
	}()

	n := &Node{
		ice:          ice,
		ctx:          ctx,
		name:         nodeID,
		maxBandwidth: maxBandwidth,
		maxListeners: maxListeners,
		updateCh:     make(chan struct{}, 1),
	}

	// The runtime configuration is just a list of mounts. Synchronize it
	// with etcd, wait until it is ready, and restart Icecast whenever it
	// changes.
	configReady, notify := client.WatchConfig(ctx, session.Client(), &n.mounts)
	go func() {
		for range notify {
			n.updateIcecast()
		}
	}()
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case <-configReady:
	}

	// Register the Icecast endpoints. First the gossip service, below
	// StatusEndpointPrefix with gossipPort, then the public Icecast
	// service.
	_, err := presence.Register(
		ctx,
		session,
		nodeID,
		presence.NewRegistration(autoradio.StatusEndpointPrefix, []net.IP{peerAddr}, gossipPort),
		presence.NewRegistration(autoradio.PublicEndpointPrefix, publicAddrs, 80),
	)
	if err != nil {
		cancel()
		return nil, err
	}

	// Run a leader election protocol advertising the peer address of our
	// Icecast daemon. We participate in the leader election, but then do
	// nothing special once we're the leader, just wait for
	// cancellation. All the state transitions are managed by the election
	// Watcher, which triggers an Icecast update.
	icecastEP := pb.NewEndpointWithIPAndPort(nodeID, []net.IP{peerAddr}, autoradio.IcecastPort)
	el := election.New(session, autoradio.IcecastElectionPrefix, icecastEP)
	go func() {
		err := el.Run(ctx, election.WaitForever)

		// If the election runner's outer retry loop terminates, we
		// are unable to talk to etcd to re-acquire leadership. In
		// this case we should abort the Node, to prevent ending up in
		// a state where all nodes are running but the cluster has no
		// leader.
		if err != nil && err != context.Canceled {
			log.Printf("leader election aborted: %v", err)
		}
		cancel()
	}()

	n.watcher = election.NewWatcher(ctx, el)
	go func() {
		for range n.watcher.Notify() {
			log.Printf("leader election state: %+v", n.watcher.State())
			n.updateIcecast()
		}
	}()

	// Start the background thread that updates Icecast whenever something
	// changes.
	go n.updateIcecastThread()

	// Start the status reporter that periodically sends our status to the
	// frontends.
	n.statusMgr = newStatusManager(ctx, session.Client())
	go util.RunCron(ctx, statusUpdateInterval, func(_ context.Context) {
		n.statusMgr.update(n.getStatus())
	})

	// Create the loadBalancer that runs within the node.
	n.lb, err = newLoadBalancer(ctx, session.Client(), nodeID, n.statusMgr, lbSpec)
	if err != nil {
		cancel()
		return nil, err
	}

	// Create the Prometheus nodeCollector.
	nc := nodeCollector{n}
	// TODO: we're currently using (due to etcd) an older
	// Prometheus client, which does not have WrapRegistererWith.
	// prometheus.WrapRegistererWith(prometheus.Labels{"name":
	// name}, prometheus.DefaultRegisterer).MustRegister(nc)
	if err := prometheus.Register(nc); err != nil {
		if _, ok := err.(prometheus.AlreadyRegisteredError); !ok {
			panic(err)
		}
	}

	return n, nil
}

// Wait for the node to terminate.
func (n *Node) Wait() {
	<-n.ctx.Done()
}

// This goroutine runs in the background and calls Update on the
// icecast.Controller whenever the updateCh channel is triggered.
func (n *Node) updateIcecastThread() {
	for range n.updateCh {
		elState := n.watcher.State()
		if elState.State == election.StateUnknown {
			log.Printf("not reloading Icecast because leadership status is unknown")
			continue
		}

		err := n.ice.Update(n.ctx, n.mounts.GetMounts(),
			(elState.State == election.StateLeader),
			elState.Leader.Addrs[0])
		if err != nil {
			log.Printf("error reloading Icecast: %v", err)
			icecastUpdateFailed.Set(1)
		} else {
			icecastUpdateFailed.Set(0)
			if elState.State == election.StateLeader {
				icecastIsLeader.Set(1)
			} else {
				icecastIsLeader.Set(0)
			}
		}

		// Sleeping here prevents us from reloading Icecast too
		// quickly when there are consecutive state changes.
		time.Sleep(1 * time.Second)
	}
}

// Trigger the updateCh in order to reload Icecast.
func (n *Node) updateIcecast() {
	select {
	case n.updateCh <- struct{}{}:
	default:
	}
}

// NumListeners returns the total number of listeners for all streams
// on this node.
func numListeners(icecastMounts []*pb.IcecastMount) int {
	var l int32
	for _, m := range icecastMounts {
		l += m.Listeners
	}
	return int(l)
}

// Return the Status protobuf for this node.
func (n *Node) getStatus() *pb.Status {
	iceMounts, iceOk := n.ice.GetStatus()
	ns := pb.Status{
		Name:          n.name,
		Timestamp:     uint64(time.Now().UTC().Unix()),
		IcecastOk:     iceOk,
		IcecastMounts: iceMounts,
		CurBandwidth:  int32(getCurrentBandwidthUsage()),
		MaxBandwidth:  int32(n.maxBandwidth),
		NumListeners:  int32(numListeners(iceMounts)),
		MaxListeners:  int32(n.maxListeners),
	}
	return &ns
}

// Return the current leader address (if any).
func (n *Node) leaderAddr() string {
	leader := n.watcher.State().Leader
	if leader == nil || len(leader.Addrs) == 0 {
		return ""
	}
	return leader.Addrs[0]
}

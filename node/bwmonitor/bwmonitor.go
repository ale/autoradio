package bwmonitor

import (
	"bufio"
	"errors"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

var spacesRx = regexp.MustCompile(`\s+`)

func getBytesSentForDevice(dev string) (uint64, error) {
	file, err := os.Open("/proc/net/dev")
	if err != nil {
		return 0, err
	}
	defer file.Close()

	input := bufio.NewScanner(file)
	for input.Scan() {
		line := spacesRx.Split(strings.TrimSpace(input.Text()), -1)
		curDev := line[0][0 : len(line[0])-1]
		if curDev == dev {
			bytes, err := strconv.ParseUint(line[9], 10, 64)
			if err != nil {
				return 0, err
			}
			return bytes, nil
		}
	}
	return 0, errors.New("device not found")
}

type BandwidthMonitor struct {
	device  string
	counter uint64
	stamp   time.Time
	period  time.Duration
	rate    float64

	lock sync.Mutex
}

func NewBandwidthMonitor(dev string) *BandwidthMonitor {
	bw := &BandwidthMonitor{
		device: dev,
		stamp:  time.Now(),
		period: 30 * time.Second,
	}
	return bw
}

func (bw *BandwidthMonitor) GetRate() float64 {
	bw.lock.Lock()
	defer bw.lock.Unlock()
	return bw.rate
}

func (bw *BandwidthMonitor) Run(stop chan bool) {
	t := time.NewTicker(bw.period)
	for {
		select {
		case now := <-t.C:
			if c, err := getBytesSentForDevice(bw.device); err == nil {
				bw.lock.Lock()
				bw.rate = float64(c-bw.counter) / now.Sub(bw.stamp).Seconds()
				bw.counter = c
				bw.stamp = now
				bw.lock.Unlock()
			}
		case <-stop:
			return
		}
	}
}

type BandwidthUsageMonitor struct {
	*BandwidthMonitor

	bwLimit float64
}

func NewBandwidthUsageMonitor(dev string, bwLimit float64) *BandwidthUsageMonitor {
	return &BandwidthUsageMonitor{
		NewBandwidthMonitor(dev),
		bwLimit,
	}
}

func (bu *BandwidthUsageMonitor) GetUsage() float64 {
	return bu.GetRate() / bu.bwLimit
}

var spplayer = {};

spplayer.init = function(p) {
    let status = "pause";
    const player = document.createElement("audio");
    const el_button = p.getElementsByClassName("button")[0];
    const el_volume = p.getElementsByClassName("volume")[0];

    // Create a source with parameters extracted from the target
    // element's attributes.
    let source = document.createElement("source");
    source.type = p.getAttribute('stream-type');
    source.src = p.getAttribute('stream-src');
    player.appendChild(source);

    // Enough of the audio has loaded to allow playback to begin.
    player.addEventListener("canplaythrough", function () {
        el_button.classList.remove("loading");
    });

    el_button.addEventListener("click", function () {
        if (status === "play") {
            player.load();
        } else {
            player.play();
        }
        status = status === "play" ? "pause" : "play";
        el_button.classList.toggle("pause");
    });

    changeVolume = function (v) {
        player.volume = el_volume.value/100;
    };

    el_volume.addEventListener("mousemove", changeVolume);
    el_volume.addEventListener("change", changeVolume);
};

spplayer.init(document.getElementById("player"));

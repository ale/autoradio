var chart = {
    palette: [
        "#268bd2",
        "#cb4b16",
        "#d33682",
        "#859900",
        "#b58900",
    ],

    setup: function(element) {
        let el = $(element);
        let queryName = el.attr('chart-query');
        let queryArg = el.attr('chart-arg');
        let queryRange = el.attr('chart-time-range');
        let title = el.attr('chart-title');
        let url = '/timeseries_query?query=' + encodeURIComponent(queryName);
        if (queryArg) {
            url += '&arg=' + encodeURIComponent(queryArg);
        }
        if (queryRange) {
            url += '&t=' + encodeURIComponent(queryRange);
        }
        $.ajax({
            url: url,
            dataType: 'json',
            success: function(data, status, xhr) {
                // Build the uPlot metadata.
                let opts = {
                    title: title,
                    width: 400,
                    height: 170,
                    series: [
                        {},
                    ],
                };
                // Incoming timestamps are milliseconds.
                for (var i = 0; i < data.data[0].length; i++) {
                    data.data[0][i] /= 1000;
                }
                for (var i = 0; i < data.labels.length; i++) {
                    opts.series.push({
                        show: true,
                        label: data.labels[i],
                        stroke: chart.palette[i % chart.palette.length],
                    });
                }
                let uplot = new uPlot(opts, data.data, element);
            },
        });
    }
};

$(function() {
    // Setup tooltips.
    $('[data-toggle="tooltip"]').tooltip();

    // Set up charts (via uPlot).
    $('.chart').each(function(idx, value) {
        chart.setup(value);
    });

    // Add callbacks to the statistics links to create dynamic charts.
    $('.stats-link').click(function() {
        var el = $(this);
        var name = el.attr('data-stream-name');
        var arg = el.attr('data-chart-arg');

        var chartEl = document.createElement('div');
        chartEl.className = 'chart';
        chartEl.setAttribute('id', 'detailChartDiv');
        chartEl.setAttribute('chart-title', 'Listeners for ' + name);
        chartEl.setAttribute('chart-query', 'stream_listeners');
        chartEl.setAttribute('chart-time-range', '6h');
        chartEl.setAttribute('chart-arg', arg);

        $('#detailChartTitle').text('Listeners for ' + name);
        $('#detailChartDiv').replaceWith(chartEl);
        $('#detailChart').show();
        chart.setup(chartEl);

        return true;
    });
});

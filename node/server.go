package node

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"git.autistici.org/ale/autoradio/node/acme"
	"go.etcd.io/etcd/client/v3"
	"golang.org/x/sync/errgroup"
)

var shutdownTimeout = 2 * time.Second

// A genericServer is just something that can be started and stopped.
type genericServer interface {
	Name() string
	Start(context.Context) error
}

// The Server runs all the request-based components of a Node. It
// bundles together servers for all the supported protocols (HTTP,
// DNS, GRPC). A failure of any of them will cause the entire Server
// to fail.
type Server struct {
	rootCtx context.Context
	cancel  context.CancelFunc
	eg      *errgroup.Group
}

func buildServer(ctx context.Context, servers ...genericServer) *Server {
	// Even if the outer context may be canceled, we want our own
	// cancel function so that we can implement Stop().
	rootCtx, cancel := context.WithCancel(ctx)
	g, innerCtx := errgroup.WithContext(rootCtx)

	for _, s := range servers {
		s := s // To lock it onto the closure for the next function.
		g.Go(func() error {
			log.Printf("starting network service: %s", s.Name())
			err := s.Start(innerCtx)
			if err != nil {
				log.Printf("%s: error: %v", s.Name(), err)
				return fmt.Errorf("%s: %v", s.Name(), err)
			}
			return nil
		})
	}

	return &Server{
		rootCtx: rootCtx,
		cancel:  cancel,
		eg:      g,
	}
}

// Stop all network services.
func (s *Server) Stop() {
	s.cancel()
}

// Wait for the services to terminate.
func (s *Server) Wait() error {
	return s.eg.Wait()
}

// Config holds all the configuration parameters for a
// Server. NewServer has too many arguments otherwise.
type Config struct {
	Domain      string
	Nameservers []string
	DNSAddrs    []net.IP
	PeerAddr    net.IP
	HTTPPort    int
	HTTPSPort   int
	DNSPort     int
	GossipPort  int
	IcecastPort int
	MetricsPort int

	ACMEEmail        string
	ACMEDirectoryURL string
	CertNames        []string
}

// NewServer creates a new Server. Will use publicAddrs / peerAddr to
// build all the necessary addr/port combinations.
//
// The main http handler will bind on all available interfaces. The
// DNS servers will bind only to the dnsAddrs (both TCP and
// UDP). The metrics and the status services, which are internal, will
// bind on peerAddr.
func NewServer(ctx context.Context, etcd *clientv3.Client, n *Node, config *Config) (*Server, error) {
	httpHandler := newHTTPHandler(n, config.IcecastPort, config.Domain)
	dnsHandler := newDNSHandler(n, config.Domain, config.Nameservers)

	var servers []genericServer

	// If HTTPS is requested, inject the ACME handler on the HTTP handler.
	if config.HTTPSPort > 0 {
		acmeMgr, err := acme.NewManager(ctx, etcd, config.ACMEEmail, config.ACMEDirectoryURL, config.CertNames)
		if err != nil {
			return nil, err
		}

		httpsHandler := httpHandler
		servers = append(servers, newHTTPSServer("https", fmt.Sprintf(":%d", config.HTTPSPort), httpsHandler, acmeMgr))
		httpHandler = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			if strings.HasPrefix(req.URL.Path, "/.well-known/acme-challenge/") {
				acmeMgr.ServeHTTP(w, req)
				return
			}
			httpsHandler.ServeHTTP(w, req)
		})
	}

	servers = append(servers, newStatusServer(mkaddr(config.PeerAddr, config.GossipPort), n.statusMgr))
	servers = append(servers, newHTTPServer("main", fmt.Sprintf(":%d", config.HTTPPort), httpHandler))
	servers = append(servers, newHTTPServer("metrics", fmt.Sprintf(":%d", config.MetricsPort), newMetricsHandler()))

	for _, ip := range config.DNSAddrs {
		servers = append(servers,
			newDNSServer("dns(udp)", mkaddr(ip, config.DNSPort), "udp", dnsHandler),
			newDNSServer("dns(tcp)", mkaddr(ip, config.DNSPort), "tcp", dnsHandler),
		)
	}

	return buildServer(ctx, servers...), nil
}

func mkaddr(ip net.IP, port int) string {
	return net.JoinHostPort(ip.String(), strconv.Itoa(port))
}

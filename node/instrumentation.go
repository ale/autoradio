package node

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// Proxy metrics.
	streamSentBytes = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "bytes_sent",
			Help: "Bytes proxied to the client, by stream.",
		},
		[]string{"stream"},
	)
	streamRcvdBytes = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "bytes_received",
			Help: "Bytes received from the client, by stream.",
		},
		[]string{"stream"},
	)
	streamListeners = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "stream_listeners",
			Help: "Number of current listeners.",
		},
		[]string{"stream"},
	)
	sourcesConnected = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "sources_connected",
			Help: "Number of connected sources per stream.",
		},
		[]string{"stream"},
	)
	proxyConnectErrs = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "proxy_connect_errors",
			Help: "Proxy connection errors (client-side).",
		},
		[]string{"stream", "upstream"},
	)
	proxyConnections = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "proxy_connections_total",
			Help: "Proxy connections.",
		},
		[]string{"proto"},
	)

	// Node metrics.
	icecastUpdateFailed = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "icecast_update_failed",
			Help: "Status of the last attempt to reload Icecast.",
		},
	)
	icecastIsLeader = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "icecast_is_leader",
			Help: "Icecast leader state.",
		},
	)

	// Status protocol (gossip) metrics.
	gossipNumNodes = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "gossip_peer_count",
			Help: "Number of peers seen by the gossip protocol.",
		},
	)
	gossipOldestTS = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "gossip_oldest_ts",
			Help: "Timestamp of the oldest update from any active peer.",
		},
	)

	// Descriptors for the nodeCollector below.
	numListenersDesc = prometheus.NewDesc(
		"status_num_listeners",
		"Number of total listeners.",
		nil, nil,
	)
	maxListenersDesc = prometheus.NewDesc(
		"status_max_listeners",
		"Maximum number of total listeners (for utilization).",
		nil, nil,
	)
	curBandwidthDesc = prometheus.NewDesc(
		"status_cur_bandwidth",
		"Current bandwidth usage (for utilization).",
		nil, nil,
	)
	maxBandwidthDesc = prometheus.NewDesc(
		"status_max_bandwidth",
		"Maximum bandwidth usage (for utilization).",
		nil, nil,
	)
	icecastOkDesc = prometheus.NewDesc(
		"status_icecast_ok",
		"Status of the node / Icecast connection.",
		nil, nil,
	)
)

// Prometheus Collector that exports the node's Status protobuf as metrics.
type nodeCollector struct {
	*Node
}

func (nc nodeCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- numListenersDesc
	ch <- maxListenersDesc
	ch <- curBandwidthDesc
	ch <- maxBandwidthDesc
	ch <- icecastOkDesc
}

func (nc nodeCollector) Collect(ch chan<- prometheus.Metric) {
	status := nc.Node.getStatus()
	ch <- prometheus.MustNewConstMetric(
		numListenersDesc,
		prometheus.GaugeValue,
		float64(status.NumListeners),
	)
	ch <- prometheus.MustNewConstMetric(
		maxListenersDesc,
		prometheus.GaugeValue,
		float64(status.MaxListeners),
	)
	ch <- prometheus.MustNewConstMetric(
		curBandwidthDesc,
		prometheus.GaugeValue,
		float64(status.CurBandwidth),
	)
	ch <- prometheus.MustNewConstMetric(
		maxBandwidthDesc,
		prometheus.GaugeValue,
		float64(status.MaxBandwidth),
	)
	var okVal float64
	if status.IcecastOk {
		okVal = 1
	}
	ch <- prometheus.MustNewConstMetric(
		icecastOkDesc,
		prometheus.GaugeValue,
		okVal,
	)
}

func init() {
	prometheus.MustRegister(
		streamSentBytes,
		streamRcvdBytes,
		streamListeners,
		sourcesConnected,
		proxyConnectErrs,
		icecastUpdateFailed,
		icecastIsLeader,
		gossipNumNodes,
		gossipOldestTS,
	)
}

func newMetricsHandler() http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	return mux
}

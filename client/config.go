package client

import (
	"context"
	"log"
	"sync"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/coordination/watcher"
	pb "git.autistici.org/ale/autoradio/proto"
	"google.golang.org/protobuf/proto"
	"go.etcd.io/etcd/client/v3"
)

// WatchableConfig mimics the watcher.Watchable interface, but with
// the decoded protobufs. This allows injecting middleware to the
// configuration state management, to implement custom notification
// protocols.
type WatchableConfig interface {
	Reset(map[string]*pb.Mount)
	Set(*pb.Mount)
	Delete(string)
}

// MountConfig is a container of mountpoints, representing the
// entirety of the autoradio database-backed configuration. It
// implements the WatchableConfig interface.
type MountConfig struct {
	mx     sync.Mutex
	mounts map[string]*pb.Mount
}

// Reset the container with new values. Implements the WatchableConfig
// interface.
func (mc *MountConfig) Reset(mounts map[string]*pb.Mount) {
	mc.mx.Lock()
	mc.mounts = mounts
	mc.mx.Unlock()
}

// Set a new mount config or update an existing one. Implements the
// WatchableConfig interface.
func (mc *MountConfig) Set(m *pb.Mount) {
	mc.mx.Lock()
	if mc.mounts == nil {
		mc.mounts = make(map[string]*pb.Mount)
	}
	mc.mounts[m.Path] = m
	mc.mx.Unlock()
}

// Delete a mount. Implements the WatchableConfig interface.
func (mc *MountConfig) Delete(path string) {
	mc.mx.Lock()
	if mc.mounts != nil {
		delete(mc.mounts, path)
	}
	mc.mx.Unlock()
}

// GetMounts returns the list of all mounts.
func (mc *MountConfig) GetMounts() []*pb.Mount {
	mc.mx.Lock()
	defer mc.mx.Unlock()
	if mc.mounts == nil {
		return nil
	}
	out := make([]*pb.Mount, 0, len(mc.mounts))
	for _, m := range mc.mounts {
		out = append(out, m)
	}
	return out
}

// GetMount returns a specific mount, with map-like semantics.
func (mc *MountConfig) GetMount(path string) (*pb.Mount, bool) {
	mc.mx.Lock()
	defer mc.mx.Unlock()
	if mc.mounts == nil {
		return nil, false
	}
	m, ok := mc.mounts[path]
	return m, ok
}

// The Watchable interface strips MountPrefix from keys. In order to
// obtain the mount path, we must prepend a slash.
func keyToPath(s string) string {
	return "/" + s
}

func pathToKey(s string) string {
	return autoradio.MountPrefix + s[1:]
}

// Decode config protobufs, converting the Watchable API to the
// WatchableConfig one.
type decoder struct {
	w WatchableConfig
}

// NewDecoder decodes mount protobufs and converts a WatchableConfig
// to a watcher.Watchable.
func NewDecoder(w WatchableConfig) watcher.Watchable {
	return &decoder{w}
}

func (d *decoder) Reset(m map[string]string) {
	mm := make(map[string]*pb.Mount)
	for k, v := range m {
		var mp pb.Mount
		if err := proto.Unmarshal([]byte(v), &mp); err != nil {
			log.Printf("config: error unmarshaling %s: %v", k, err)
			continue
		}
		path := keyToPath(k)
		if path != mp.Path {
			// This would be weird.
			log.Printf("config: error: mount.Path (%s) != key (%s)", mp.Path, path)
			continue
		}
		mm[path] = &mp
	}
	d.w.Reset(mm)
}

func (d *decoder) Set(k, v string) {
	var mp pb.Mount
	if err := proto.Unmarshal([]byte(v), &mp); err != nil {
		log.Printf("config: error unmarshaling %s: %v", k, err)
		return
	}
	if path := keyToPath(k); path != mp.Path {
		// This would be weird.
		log.Printf("config: error: mount.Path (%s) != key (%s)", mp.Path, path)
		return
	}
	d.w.Set(&mp)
}

func (d *decoder) Delete(k string) {
	d.w.Delete(keyToPath(k))
}

// WatchConfig monitors a WatchableConfig object and keeps it in sync
// with etcd. The outer Context is used for termination.  Returns the
// WaitForInit() and Notify() channels so the client can know when the
// configuration is bootstrapped and changes.
//
// The simplest, read-only configuration management looks like this:
//
//     var config client.MountConfig
//     ready, _ := client.WatchConfig(ctx, etcdClient, &config)
//     <-ready
//
func WatchConfig(ctx context.Context, cli *clientv3.Client, target WatchableConfig) (<-chan struct{}, <-chan struct{}) {
	nw := watcher.NewNotifyWatchable(ctx, NewDecoder(target))
	w := watcher.NewReadyWatchable(nw)
	go watcher.Watch(ctx, cli, autoradio.MountPrefix, w)
	return w.WaitForInit(), nw.Notify()
}

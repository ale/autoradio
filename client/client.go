package client

import (
	"context"
	"errors"

	"google.golang.org/protobuf/proto"
	"go.etcd.io/etcd/client/v3"

	"git.autistici.org/ale/autoradio"
	pb "git.autistici.org/ale/autoradio/proto"
)

// ErrNotFound is returned by GetMount when an object does not exist.
var ErrNotFound = errors.New("not found")

// The Client manipulates the autoradio stream configuration by
// talking directly to etcd.
type Client struct {
	cli *clientv3.Client
}

// New returns a new Client.
func New(cli *clientv3.Client) *Client {
	return &Client{cli}
}

// GetMount returns data on a specific mountpoint.
func (c *Client) GetMount(ctx context.Context, path string) (*pb.Mount, error) {
	resp, err := c.cli.Get(ctx, pathToKey(path))
	if err != nil {
		return nil, err
	}
	if len(resp.Kvs) < 1 {
		return nil, ErrNotFound
	}
	var m pb.Mount
	if err := proto.Unmarshal(resp.Kvs[0].Value, &m); err != nil {
		return nil, err
	}
	return &m, nil
}

// SetMount creates or updates an existing mountpoint.
func (c *Client) SetMount(ctx context.Context, m *pb.Mount) error {
	data, err := proto.Marshal(m)
	if err != nil {
		return err
	}
	_, err = c.cli.Put(ctx, pathToKey(m.Path), string(data))
	return err
}

// DeleteMount deletes a mountpoint.
func (c *Client) DeleteMount(ctx context.Context, path string) error {
	_, err := c.cli.Delete(ctx, pathToKey(path))
	return err
}

// ListMounts returns a list of all mountpoints.
func (c *Client) ListMounts(ctx context.Context) ([]*pb.Mount, error) {
	resp, err := c.cli.Get(ctx, autoradio.MountPrefix, clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortAscend))
	if err != nil {
		return nil, err
	}
	var out []*pb.Mount
	for _, ev := range resp.Kvs {
		var m pb.Mount
		if err := proto.Unmarshal(ev.Value, &m); err != nil {
			continue
		}
		out = append(out, &m)
	}
	return out, nil
}

package transcoder

import (
	"context"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"strings"
	"time"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/coordination/election"
	pb "git.autistici.org/ale/autoradio/proto"
	"go.etcd.io/etcd/client/v3/concurrency"
)

type transcoder struct {
	mount *pb.Mount

	cancel context.CancelFunc
	done   chan struct{}
}

func newTranscoder(ctx context.Context, session *concurrency.Session, m *pb.Mount, nodeID string) *transcoder {
	// Create a subcontext so we can cancel the execution of the
	// transcoder at any point in time.
	tctx, cancel := context.WithCancel(ctx)

	t := &transcoder{
		mount:  m,
		cancel: cancel,
		done:   make(chan struct{}),
	}

	// Start a leader election to run a single transcoder
	// globally.  Sleep a small random amount of time before
	// participating in the election, so that leadership is more
	// easily spread around nodes. The election prefix should not
	// contain nested directories (these are key prefixes, not
	// paths), so we perform a simple character substitution to
	// obtain the mount-specific election prefix. Furthermore,
	// the election API requires an Endpoint proto, but we aren't
	// providing any network service, so we just create a weird
	// Endpoint without Addrs.x
	electionPath := autoradio.TranscoderElectionPrefix + strings.Replace(m.Path, "/", "_", -1) + "/"
	el := election.New(session, electionPath, &pb.Endpoint{
		Name: nodeID,
	})
	go func() {
		sleepTinyBit()
		err := el.Run(tctx, t.run)
		if err != nil && err != context.Canceled {
			log.Printf("transcoder failed: %s: %v", m.Path, err)
		}
		close(t.done)
	}()

	return t
}

func (t *transcoder) stop() {
	t.cancel()
	<-t.done
}

func (t *transcoder) run(ctx context.Context) error {
	// Write the liquidsoap configuration for this stream to a
	// temporary file.
	config, err := makeLiquidsoapConfig(t.mount)
	if err != nil {
		return err
	}
	configFile, err := ioutil.TempFile("", "liquidsoap-")
	if err != nil {
		return err
	}
	defer os.Remove(configFile.Name())
	_, err = configFile.Write(config)
	configFile.Close()
	if err != nil {
		return err
	}

	log.Printf("starting transcode of %s to %s", t.mount.TranscodeParams.SourcePath, t.mount.Path)

	// Start liquidsoap and run until the context is canceled.
	cmd := exec.CommandContext(ctx, *liquidsoapBin, "-T", "-U", "-v", configFile.Name())
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil && err != context.Canceled {
		// Sleeping here limits the rate of liquidsoap restart
		// if we are crashlooping for whatever reason.
		time.Sleep(1 * time.Second)
		log.Printf("error: %s: liquidsoap failed: %v", t.mount.Path, err)
	}
	return err
}

func sleepTinyBit() {
	// 1 +/- 0.5 seconds.
	offNano := 500000000 + rand.Intn(1000000000)
	tinyBit := time.Duration(offNano) * time.Nanosecond
	time.Sleep(tinyBit)
}

package transcoder

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"sync"
	"text/template"

	pb "git.autistici.org/ale/autoradio/proto"
)

var (
	liquidsoapBin          = flag.String("liquidsoap", "/usr/bin/liquidsoap", "Location of the liquidsoap binary")
	liquidsoapTemplateFile = flag.String("liquidsoap-template", "", "Custom liquidsoap config template (if empty, use builtin default)")

	liquidsoapConfigStr = `
set("log.file", false)
set("log.stdout", true)
set("log.unix_timestamps", false)

upstream = mksafe(input.http("{{.SourceURL}}", buffer=5.0))
{{if eq .Channels 1}}upstream = mean(upstream){{end}}
output.icecast({{.Format}},
  mount="{{.TargetMount}}", host="{{.TargetHost}}", port={{.TargetPort}}, user="{{.TargetUsername}}", password="{{.TargetPassword}}",
  upstream)
`

	liquidsoapConfigTpl *template.Template
	liquidsoapTplInit   sync.Once
)

func getLiquidsoapConfigTemplate() *template.Template {
	liquidsoapTplInit.Do(func() {
		tplStr := liquidsoapConfigStr
		if *liquidsoapTemplateFile != "" {
			data, err := ioutil.ReadFile(*liquidsoapTemplateFile)
			if err != nil {
				panic(err)
			}
			tplStr = string(data)
		}
		liquidsoapConfigTpl = template.Must(template.New("liquidsoap").Parse(tplStr))
	})
	return liquidsoapConfigTpl
}

// Parameters to configure a liquidsoap-based transcoder.
type liquidsoapParams struct {
	// Source (upstream) URL.
	SourceURL string

	// Target (downstream) connection parameters.
	TargetHost     string
	TargetPort     int
	TargetMount    string
	TargetUsername string
	TargetPassword string

	Format   string
	Channels int
}

// Create new parameters for liquidsoap for a transcoding mount. If
// mount.TranscodeParams is nil, this function will panic, so the
// caller should check mount.Transcode.
//
// The source URL points at localhost, relying on the front-end
// proxying to actually send the traffic to the master. This has the
// advantage that we don't have to reconfigure liquidsoap on
// mastership changes.
func newLiquidsoapParams(m *pb.Mount) *liquidsoapParams {
	return &liquidsoapParams{
		SourceURL:      fmt.Sprintf("http://localhost%s", m.TranscodeParams.SourcePath),
		TargetHost:     "localhost",
		TargetPort:     80,
		TargetMount:    m.Path,
		TargetUsername: m.SourceUsername,
		TargetPassword: m.SourcePassword,
		Format:         liquidsoapFormatString(m.TranscodeParams),
		Channels:       int(m.TranscodeParams.Channels),
	}
}

func makeLiquidsoapConfig(m *pb.Mount) ([]byte, error) {
	var buf bytes.Buffer
	if err := getLiquidsoapConfigTemplate().Execute(&buf, newLiquidsoapParams(m)); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func liquidsoapFormatString(params *pb.EncodingParams) string {
	var outp []string
	switch params.Format {
	case "mp3", "mp3.cbr", "mp3.vbr", "mp3.abr":
		if params.Channels == 1 {
			outp = append(outp, "mono")
		} else {
			outp = append(outp, "stereo")
			if params.StereoMode != "" {
				outp = append(outp, fmt.Sprintf("stereo_mode=\"%s\"", params.StereoMode))
			}
		}
		outp = append(outp, fmt.Sprintf("samplerate=%d", params.SampleRate))
		switch params.Format {
		case "mp3", "mp3.cbr", "mp3.abr":
			outp = append(outp, fmt.Sprintf("bitrate=%d", params.BitRate))
		case "mp3.vbr":
			outp = append(outp, fmt.Sprintf("quality=%d", int(params.Quality)))
		}
	case "vorbis", "vorbis.cbr", "vorbis.abr":
		outp = append(outp, fmt.Sprintf("channels=%d", params.Channels))
		outp = append(outp, fmt.Sprintf("samplerate=%d", params.SampleRate))
		switch params.Format {
		case "vorbis":
			outp = append(outp, fmt.Sprintf("quality=%g", params.Quality))
		case "vorbis.abr", "vorbis.cbr":
			outp = append(outp, fmt.Sprintf("bitrate=%d", params.BitRate))
		}
	}
	return fmt.Sprintf("%%%s(%s)", params.Format, strings.Join(outp, ", "))
}

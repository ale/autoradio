package transcoder

import (
	"context"

	"git.autistici.org/ale/autoradio/client"
	pb "git.autistici.org/ale/autoradio/proto"
	"google.golang.org/protobuf/proto"
	"go.etcd.io/etcd/client/v3/concurrency"
)

type Manager struct {
	ctx     context.Context
	session *concurrency.Session
	nodeID  string

	transcoders map[string]*transcoder
}

func New(ctx context.Context, session *concurrency.Session, nodeID string) *Manager {
	m := &Manager{
		ctx:         ctx,
		session:     session,
		nodeID:      nodeID,
		transcoders: make(map[string]*transcoder),
	}
	configReady, _ := client.WatchConfig(ctx, session.Client(), m)
	<-configReady
	return m
}

func (t *Manager) Reset(mounts map[string]*pb.Mount) {
	// Figure out what the differences are.
	newTC := make(map[string]*transcoder)
	for path, m := range mounts {
		if !m.HasTranscoder() {
			continue
		}
		tc, ok := t.transcoders[path]
		if !ok || !proto.Equal(tc.mount, m) {
			tc = newTranscoder(t.ctx, t.session, m, t.nodeID)
		}
		delete(t.transcoders, path)
		newTC[path] = tc
	}
	for _, tc := range t.transcoders {
		tc.stop()
	}

	t.transcoders = newTC
}

func (t *Manager) Set(m *pb.Mount) {
	tc, ok := t.transcoders[m.Path]
	if ok && proto.Equal(tc.mount, m) {
		return
	}
	if ok {
		tc.stop()
	}
	tc = newTranscoder(t.ctx, t.session, m, t.nodeID)
	t.transcoders[m.Path] = tc
}

func (t *Manager) Delete(path string) {
	tc, ok := t.transcoders[path]
	if ok {
		tc.stop()
		delete(t.transcoders, path)
	}
}

func (t *Manager) Close() {
	for _, tc := range t.transcoders {
		tc.stop()
	}
}

autoradio (1.99) unstable; urgency=medium

  * First 2.0 pre-release.

 -- ale <ale@incal.net>  Thu, 11 Apr 2019 22:01:38 +0100

autoradio (0.7.2) unstable; urgency=medium

  * Add support for static redirects.

 -- ale <ale@incal.net>  Wed, 23 Mar 2016 21:48:52 +0000

autoradio (0.7.1p2) unstable; urgency=medium

  * Small fix to public URLs in dashboard.

 -- ale <ale@incal.net>  Sat, 05 Dec 2015 11:44:54 +0000

autoradio (0.7.1p1) unstable; urgency=medium

  * Fix crashing bug with liquidsoap 1.1.

 -- ale <ale@incal.net>  Tue, 01 Dec 2015 13:09:13 +0000

autoradio (0.7.1) unstable; urgency=medium

  * Serve authoritative NS records.

 -- ale <ale@incal.net>  Tue, 24 Nov 2015 09:34:37 +0000

autoradio (0.7) unstable; urgency=medium

  * New version for Debian Jessie.

 -- ale <ale@incal.net>  Mon, 02 Nov 2015 10:23:48 +0000

autoradio (0.6.1p2) unstable; urgency=medium

  * Replace some upstream dependencies.

 -- ale <ale@incal.net>  Sat, 17 Oct 2015 00:39:01 +0100

autoradio (0.6.1p1) unstable; urgency=medium

  * A number of minor fixes to ipv4/ipv6 correctness.
  * --enable-icecast-proxy is now the default.

 -- ale <ale@incal.net>  Sat, 17 Oct 2015 00:00:51 +0100

autoradio (0.6.1) unstable; urgency=medium

  * Rebuilt with Go 1.5.1.
  * Fixes to the presence library.

 -- ale <ale@incal.net>  Thu, 20 Aug 2015 08:38:48 +0100

autoradio (0.6) unstable; urgency=medium

  * Etcd 2.0 compatibility.

 -- ale <ale@incal.net>  Sat, 25 Jul 2015 08:37:59 +0100

autoradio (0.5.2) unstable; urgency=medium

  * Minor fixes to --restrict-debug.
  * Bumped minor version after the last batch of fixes.

 -- ale <ale@incal.net>  Fri, 24 Jul 2015 08:48:17 +0100

autoradio (0.5.1p4) unstable; urgency=medium

  * Fix liquidsoap controller robustness issues.

 -- ale <ale@incal.net>  Wed, 22 Jul 2015 12:46:39 +0100

autoradio (0.5.1p3) unstable; urgency=medium

  * Switch back to simple TCP proxy for /_stream.

 -- ale <ale@incal.net>  Sun, 12 Jul 2015 19:11:20 +0100

autoradio (0.5.1p2) unstable; urgency=medium

  * Fix crash when losing transcoder mastership.

 -- ale <ale@incal.net>  Sat, 31 Jan 2015 09:04:17 +0000

autoradio (0.5.1p1) unstable; urgency=medium

  * Fix crash in masterelection.

 -- ale <ale@incal.net>  Tue, 27 Jan 2015 09:45:41 +0000

autoradio (0.5.1) unstable; urgency=medium

  * Reduce number of useless icecast restarts.
  * More /debug/ handlers.

 -- ale <ale@incal.net>  Tue, 20 Jan 2015 08:33:55 +0000

autoradio (0.5p3) unstable; urgency=medium

  * Fixed crash in radioctl.

 -- ale <ale@incal.net>  Fri, 16 Jan 2015 20:08:37 +0000

autoradio (0.5p2) unstable; urgency=medium

  * Fixed radioctl edit-mount.

 -- ale <ale@incal.net>  Fri, 16 Jan 2015 18:22:14 +0000

autoradio (0.5p1) unstable; urgency=medium

  * Fixes to liquidsoap transcoding.

 -- ale <ale@incal.net>  Thu, 15 Jan 2015 18:18:08 +0000

autoradio (0.5) unstable; urgency=medium

  * Transcoding support.

 -- ale <ale@incal.net>  Mon, 12 Jan 2015 08:01:27 +0000

autoradio (0.4) unstable; urgency=medium

  * Robustness and correctness improvements.

 -- ale <ale@incal.net>  Sat, 03 Jan 2015 09:34:10 +0000

autoradio (0.3.6p4) unstable; urgency=medium

  * Use godep to build the package.

 -- ale <ale@incal.net>  Wed, 24 Dec 2014 10:07:46 +0000

autoradio (0.3.6p3) unstable; urgency=medium

  * Try harder to prevent caching of redirects.

 -- ale <ale@incal.net>  Fri, 14 Nov 2014 13:01:26 +0000

autoradio (0.3.6p2) unstable; urgency=medium

  * Try to prevent caching of 302 responses.

 -- ale <ale@incal.net>  Fri, 07 Nov 2014 15:33:26 +0000

autoradio (0.3.6p1) unstable; urgency=medium

  * Increased burst size.

 -- ale <ale@incal.net>  Wed, 05 Nov 2014 09:45:37 +0000

autoradio (0.3.6) unstable; urgency=medium

  * More reliable stream proxying.

 -- ale <ale@incal.net>  Sat, 18 Oct 2014 22:19:20 +0100

autoradio (0.3.5) unstable; urgency=medium

  * Icecast proxy.

 -- ale <ale@incal.net>  Fri, 17 Oct 2014 08:44:42 +0100

autoradio (0.3.4p1) unstable; urgency=medium

  * Fix to the masterelection protocol.

 -- ale <ale@incal.net>  Wed, 15 Oct 2014 07:59:14 +0100

autoradio (0.3.4) unstable; urgency=medium

  * Updated radioctl.

 -- ale <ale@incal.net>  Sun, 12 Oct 2014 12:54:11 +0100

autoradio (0.3.3) unstable; urgency=medium

  * Serve 302 redirects for direct stream URLs instead of M3Us.

 -- ale <ale@incal.net>  Sun, 12 Oct 2014 10:07:21 +0100

autoradio (0.3.2p2) unstable; urgency=medium

  * Reliability fixes.

 -- ale <ale@incal.net>  Sat, 11 Oct 2014 12:01:41 +0100

autoradio (0.3.2p1) unstable; urgency=medium

  * Instrumentation fix.

 -- ale <ale@incal.net>  Thu, 09 Oct 2014 08:45:50 +0100

autoradio (0.3.2) unstable; urgency=medium

  * Add statsd instrumentation.

 -- ale <ale@incal.net>  Wed, 08 Oct 2014 17:25:57 +0100

autoradio (0.3.1) unstable; urgency=medium

  * Add bwmonitor and associated load balancing policy.
  * Etcd client updated.

 -- ale <ale@incal.net>  Thu, 02 Oct 2014 11:40:15 +0100

autoradio (0.3) unstable; urgency=low

  * Updated to use the etcd 0.3 API.

 -- ale <ale@incal.net>  Thu, 20 Feb 2014 11:13:48 +0000

autoradio (0.2-2) unstable; urgency=low

  * Attempt to increase compatibility of the generated icecast configuration.

 -- ale <ale@incal.net>  Sun, 22 Dec 2013 19:07:30 +0000

autoradio (0.2-1) unstable; urgency=low

  * Fixes to packaging scripts.

 -- ale <ale@incal.net>  Sun, 22 Dec 2013 18:36:23 +0000

autoradio (0.2) unstable; urgency=low

  * Update to the etcd 0.2 API.

 -- ale <ale@incal.net>  Fri, 15 Nov 2013 20:40:06 +0000

autoradio (0.1) unstable; urgency=low

  * Initial Release.

 -- ale <ale@incal.net>  Thu, 07 Nov 2013 09:00:44 +0000

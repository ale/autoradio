package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/client"
	pb "git.autistici.org/ale/autoradio/proto"
	"github.com/google/subcommands"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc"
)

// Format for output of structured data.
var (
	etcdEndpoints = flag.String("etcd", "http://localhost:2379", "Etcd endpoints (comma-separated list of URLs)")
	outputFormat  = flag.String("format", "txt", "Output format for structured data (json, txt)")
)

type optionalValue struct {
	isSet bool
}

func (v optionalValue) IsSet() bool {
	return v.isSet
}

type stringOptionalValue struct {
	optionalValue
	value string
}

func (v *stringOptionalValue) Set(val string) error {
	v.isSet = true
	v.value = val
	return nil
}

func (v *stringOptionalValue) String() string {
	return v.value
}

type intOptionalValue struct {
	optionalValue
	value int
}

func (v *intOptionalValue) Set(val string) error {
	ival, err := strconv.Atoi(val)
	v.value = ival
	v.isSet = true
	return err
}

func (v *intOptionalValue) String() string {
	return fmt.Sprintf("%v", v.value)
}

type floatOptionalValue struct {
	optionalValue
	value float64
}

func (v *floatOptionalValue) Set(val string) error {
	fval, err := strconv.ParseFloat(val, 64)
	v.value = fval
	v.isSet = true
	return err
}

func (v *floatOptionalValue) String() string {
	return fmt.Sprintf("%v", v.value)
}

var auClient *client.Client

func getClient() *client.Client {
	if auClient == nil {
		cli, err := clientv3.New(clientv3.Config{
			Endpoints:   strings.Split(*etcdEndpoints, ","),
			DialOptions: []grpc.DialOption{grpc.WithBlock()},
			DialTimeout: 5 * time.Second,
		})
		if err != nil {
			log.Fatalf("failed to connect to etcd: %v", err)
		}
		auClient = client.New(cli)
	}
	return auClient
}

func setRelay(m *pb.Mount, relayURL string) {
	if relayURL == "" {
		// Randomly generate source credentials.
		m.SourceUsername = autoradio.GenerateUsername(m.Path)
		m.SourcePassword = autoradio.GeneratePassword()
	} else {
		// Validate the given relay URL.
		u, err := url.Parse(relayURL)
		if err != nil {
			log.Fatal(err)
		}
		m.RelayUrl = u.String()
		m.SourceUsername = ""
		m.SourcePassword = ""
	}
}

func printMount(m *pb.Mount) {
	switch *outputFormat {
	case "json":
		s, _ := json.MarshalIndent(m, "", "    ")
		os.Stdout.Write(s)

	case "txt":
		fmt.Printf("path=%s\n", m.Path)
		if m.SourceUsername != "" {
			fmt.Printf("username=%s\npassword=%s\n", m.SourceUsername, m.SourcePassword)
		}
		if m.RelayUrl != "" {
			fmt.Printf("relay_url=%s\n", m.RelayUrl)
		}
		if m.FallbackPath != "" {
			fmt.Printf("fallback_url=%s\n", m.FallbackPath)
		}
		if t := m.TranscodeParams; t != nil {
			fmt.Printf("transcode_source_url=%s\n", t.SourcePath)
			fmt.Printf("transcode_format=%s\n", t.Format)
			fmt.Printf("transcode_bitrate=%d\n", t.BitRate)
			fmt.Printf("transcode_quality=%f\n", t.Quality)
			fmt.Printf("transcode_samplerate=%d\n", t.SampleRate)
		}

	default:
		log.Printf("unsupported output format \"%s\"", *outputFormat)
	}
}

func mountExists(ctx context.Context, name string, c *client.Client) bool {
	m, _ := c.GetMount(ctx, name)
	return m != nil
}

// Create a new mountpoint.
type createMountCommand struct {
	relay    string
	fallback string
}

func (c *createMountCommand) Name() string     { return "create-mount" }
func (c *createMountCommand) Synopsis() string { return "Create a new mountpoint" }
func (c *createMountCommand) Usage() string {
	return `create-mount <path>

Create a new mountpoint at the specified PATH (which will become the
absolute URL of the stream).

`
}

func (c *createMountCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.relay, "relay", "", "Upstream URL to relay")
	f.StringVar(&c.fallback, "fallback", "", "Fallback stream URL")
}

func (c *createMountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	path := f.Arg(0)
	if !strings.HasPrefix(path, "/") {
		log.Printf("Warning: mountpoint %s does not start with a slash, using /%s instead", path, path)
		path = "/" + path
	}

	// Check if the mount already exists.
	client := getClient()
	if mountExists(ctx, path, client) {
		log.Printf("ERROR: A mount with that name already exists!")
		return subcommands.ExitFailure
	}

	// Create the new mount and set the relevant fields (depending
	// on the options passed to the command).
	m := &pb.Mount{Path: path}
	setRelay(m, c.relay)
	m.FallbackPath = c.fallback

	if err := m.Valid(); err != nil {
		log.Printf("ERROR: mount configuration is invalid: %v", err)
		return subcommands.ExitFailure
	}

	if err := client.SetMount(ctx, m); err != nil {
		log.Printf("ERROR: creating mount: %v", err)
		return subcommands.ExitFailure
	}

	printMount(m)
	return subcommands.ExitSuccess
}

// Create a submount (transcoded stream).
type createTranscodingMountCommand struct {
	sourcePath string
	format     string
	quality    float64
	bitRate    int
	sampleRate int
	channels   int
	stereoMode string
	fallback   string
}

func (c *createTranscodingMountCommand) Name() string     { return "create-transcoding-mount" }
func (c *createTranscodingMountCommand) Synopsis() string { return "Create a transcoding mount" }
func (c *createTranscodingMountCommand) Usage() string {
	return `create-transcoding-mount <path>

Create a new stream that will transcode the parent stream with
different encoding parameters.

`
}

func (c *createTranscodingMountCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.sourcePath, "source", "", "Source mountpoint")
	f.StringVar(&c.format, "codec", "mp3", "Encoding format")
	f.Float64Var(&c.quality, "quality", 0, "Quality (for VBR encoders)")
	f.IntVar(&c.bitRate, "bitrate", 32, "Bitrate (Kbps)")
	f.IntVar(&c.sampleRate, "samplerate", 44100, "Sample rate (Hz)")
	f.IntVar(&c.channels, "channels", 2, "Number of channels")
	f.StringVar(&c.stereoMode, "stereo-mode", "joint_stereo", "Stereo mode for mp3 codec (stereo, joint_stereo)")
	f.StringVar(&c.fallback, "fallback", "", "Fallback stream URL")
}

func (c *createTranscodingMountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	path := f.Arg(0)
	if !strings.HasPrefix(path, "/") {
		log.Printf("Warning: mountpoint %s does not start with a slash, using /%s instead", path, path)
		path = "/" + path
	}

	// The mount path should not exist.
	client := getClient()
	if mountExists(ctx, path, client) {
		log.Printf("ERROR: a mount with that name already exists!")
		return subcommands.ExitFailure
	}
	// The source mount should exist.
	if !mountExists(ctx, c.sourcePath, client) {
		log.Printf("ERROR: the source mount does not exist!")
		return subcommands.ExitFailure
	}

	// Retrieve the parent mount point and add a TranscodingMount.
	m := &pb.Mount{
		Path:         path,
		FallbackPath: c.fallback,
		Transcode:    true,
		TranscodeParams: &pb.EncodingParams{
			SourcePath: c.sourcePath,
			Format:     c.format,
			BitRate:    int32(c.bitRate),
			SampleRate: int32(c.sampleRate),
			Channels:   int32(c.channels),
			StereoMode: c.stereoMode,
			Quality:    float32(c.quality),
		},
	}
	setRelay(m, "")

	if err := m.Valid(); err != nil {
		log.Printf("ERROR: mount configuration is invalid: %v", err)
		return subcommands.ExitFailure
	}

	if err := client.SetMount(ctx, m); err != nil {
		log.Printf("ERROR: creating mount: %v", err)
		return subcommands.ExitFailure
	}

	printMount(m)
	return subcommands.ExitSuccess
}

// Edit a mountpoint.
type editMountCommand struct {
	relay           stringOptionalValue
	fallback        stringOptionalValue
	transFormat     stringOptionalValue
	transSource     stringOptionalValue
	transBitRate    intOptionalValue
	transSampleRate intOptionalValue
	transChannels   intOptionalValue
	transStereoMode stringOptionalValue
	transQuality    floatOptionalValue
}

func (c *editMountCommand) Name() string     { return "edit-mount" }
func (c *editMountCommand) Synopsis() string { return "Edit an existing mountpoint" }
func (c *editMountCommand) Usage() string {
	return `edit-mount <path>

Modify parameters of the specified mountpoint, such as the relay
and the fallback URL. If the relay option is set, the mountpoint
will not accept source connections anymore. To revert to the
default, non-relay behavior, set the relay to the empty string
(with --relay="").

`
}

func (c *editMountCommand) SetFlags(f *flag.FlagSet) {
	f.Var(&c.relay, "relay", "Upstream URL to relay")
	f.Var(&c.fallback, "fallback", "Fallback stream URL")

	f.Var(&c.transSource, "source", "[transcoding] Source mountpoint")
	f.Var(&c.transFormat, "codec", "[transcoding] Encoding format")
	f.Var(&c.transQuality, "quality", "[transcoding] Quality (for VBR encoders)")
	f.Var(&c.transBitRate, "bitrate", "[transcoding] Bitrate (Kbps)")
	f.Var(&c.transSampleRate, "samplerate", "[transcoding] Sample rate (Hz)")
	f.Var(&c.transChannels, "channels", "[transcoding] Number of channels")
	f.Var(&c.transStereoMode, "stereo-mode", "[transcoding] Stereo mode for mp3 encoding (stereo, joint_stereo)")
}

func (c *editMountCommand) transcodingOptionsSet() bool {
	opts := []interface {
		IsSet() bool
	}{
		&c.transSource, &c.transFormat, &c.transQuality,
		&c.transBitRate, &c.transSampleRate, &c.transChannels,
		&c.transStereoMode,
	}
	for _, o := range opts {
		if o.IsSet() {
			return true
		}
	}
	return false
}

func (c *editMountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	client := getClient()
	m, err := client.GetMount(ctx, f.Arg(0))
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	if m == nil {
		log.Printf("ERROR: mount not found")
		return subcommands.ExitFailure
	}

	// Only set those fields that were passed on the command line.
	if c.fallback.IsSet() {
		m.FallbackPath = c.fallback.value
	}
	if c.relay.IsSet() {
		setRelay(m, c.relay.value)
	}

	if c.transcodingOptionsSet() && !m.Transcode {
		log.Printf("ERROR: can't set transcoding options on a non-transcoding mount (delete and re-create)")
		return subcommands.ExitFailure
	}

	if c.transFormat.IsSet() {
		m.TranscodeParams.Format = c.transFormat.value
	}
	if c.transSource.IsSet() {
		m.TranscodeParams.SourcePath = c.transSource.value
	}
	if c.transBitRate.IsSet() {
		m.TranscodeParams.BitRate = int32(c.transBitRate.value)
	}
	if c.transSampleRate.IsSet() {
		m.TranscodeParams.SampleRate = int32(c.transSampleRate.value)
	}
	if c.transQuality.IsSet() {
		m.TranscodeParams.Quality = float32(c.transQuality.value)
	}
	if c.transChannels.IsSet() {
		m.TranscodeParams.Channels = int32(c.transChannels.value)
	}
	if c.transStereoMode.IsSet() {
		m.TranscodeParams.StereoMode = c.transStereoMode.value
	}

	if err := m.Valid(); err != nil {
		log.Printf("ERROR: mount configuration is invalid: %v", err)
		return subcommands.ExitFailure
	}

	if err := client.SetMount(ctx, m); err != nil {
		log.Printf("ERROR: updating mount: %v", err)
		return subcommands.ExitFailure
	}

	printMount(m)
	return subcommands.ExitSuccess
}

// Delete an existing mountpoint.
type deleteMountCommand struct {
}

func (c *deleteMountCommand) Name() string     { return "delete-mount" }
func (c *deleteMountCommand) Synopsis() string { return "Delete a mountpoint" }
func (c *deleteMountCommand) Usage() string {
	return `delete-mount <path>

Delete the specified mountpoint.
`
}

func (c *deleteMountCommand) SetFlags(_ *flag.FlagSet) {}

func (c *deleteMountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}
	path := f.Arg(0)
	client := getClient()
	if !mountExists(ctx, path, client) {
		log.Printf("ERROR: mount not found")
		return subcommands.ExitFailure
	}

	if err := client.DeleteMount(ctx, path); err != nil {
		log.Printf("ERROR: deleting mount: %v", err)
		return subcommands.ExitFailure
	}

	// Delete all the transcoding mounts that have this as a
	// source.
	mounts, err := client.ListMounts(ctx)
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	for _, m := range mounts {
		if m.HasTranscoder() && m.TranscodeParams.SourcePath == path {
			if err := client.DeleteMount(ctx, m.Path); err != nil {
				log.Printf("ERROR: deleting transcoded mount %s: %v", m.Path, err)
				return subcommands.ExitFailure
			}
		}
	}

	log.Printf("mountpoint %s removed", path)
	return subcommands.ExitSuccess
}

// List known mountpoints.
type listMountsCommand struct{}

func (c *listMountsCommand) Name() string     { return "list-mounts" }
func (c *listMountsCommand) Synopsis() string { return "List all configured mountpoints" }
func (c *listMountsCommand) Usage() string {
	return `list-mounts

Outputs a list of all the currently configured mountpoints.
`
}

func (c *listMountsCommand) SetFlags(_ *flag.FlagSet) {}

func (c *listMountsCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	mounts, err := getClient().ListMounts(ctx)
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	var names []string
	for _, m := range mounts {
		names = append(names, m.Path)
	}

	switch *outputFormat {
	case "json":
		s, _ := json.MarshalIndent(names, "", "    ")
		os.Stdout.Write(s)

	//case "txt":
	default:
		for _, n := range names {
			fmt.Println(n)
		}
	}

	return subcommands.ExitSuccess
}

// Show mountpoint information.
type showMountCommand struct{}

func (c *showMountCommand) Name() string     { return "show-mount" }
func (c *showMountCommand) Synopsis() string { return "Show mountpoint information" }
func (c *showMountCommand) Usage() string {
	return `show-mount <path>

Print information about the specified mountpoint (including
the source credentials).
`
}

func (c *showMountCommand) SetFlags(_ *flag.FlagSet) {}

func (c *showMountCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	m, err := getClient().GetMount(ctx, f.Arg(0))
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	if m == nil {
		log.Printf("ERROR: mount not found")
		return subcommands.ExitFailure
	}

	printMount(m)
	return subcommands.ExitSuccess
}

// Backup mount configuration.
type backupCommand struct{}

func (c *backupCommand) Name() string     { return "backup" }
func (c *backupCommand) Synopsis() string { return "Backup mount configuration" }
func (c *backupCommand) Usage() string {
	return `backup

Dump the autoradio configuration to stdout, in a format that is
understood by the "restore" command.
`
}

func (c *backupCommand) SetFlags(_ *flag.FlagSet) {}

func (c *backupCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	mounts, err := getClient().ListMounts(ctx)
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	if err := json.NewEncoder(os.Stdout).Encode(mounts); err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

// Legacy EncodingParams type from autoradio v2.
type legacyEncodingParams struct {
	SourceName string
	Format     string
	BitRate    int32
	SampleRate int32
	Channels   int32
	StereoMode string
	Quality    float32
}

// Legacy Mount type from autoradio v2.
type legacyMount struct {
	Name        string
	Username    string
	Password    string
	RelayUrl    string
	Fallback    string
	Transcoding *legacyEncodingParams
}

func (m *legacyMount) toProto() *pb.Mount {
	out := &pb.Mount{
		Path:           m.Name,
		SourceUsername: m.Username,
		SourcePassword: m.Password,
		RelayUrl:       m.RelayUrl,
		FallbackPath:   m.Fallback,
	}
	if m.Transcoding != nil {
		out.Transcode = true
		out.TranscodeParams = &pb.EncodingParams{
			SourcePath: m.Transcoding.SourceName,
			Format:     m.Transcoding.Format,
			BitRate:    m.Transcoding.BitRate,
			SampleRate: m.Transcoding.SampleRate,
			Channels:   m.Transcoding.Channels,
			StereoMode: m.Transcoding.StereoMode,
			Quality:    m.Transcoding.Quality,
		}
	}
	return out
}

// Restore mount configuration.
type restoreCommand struct {
	v2compat bool
}

func (c *restoreCommand) Name() string     { return "restore" }
func (c *restoreCommand) Synopsis() string { return "Restore mount configuration" }
func (c *restoreCommand) Usage() string {
	return `restore

Read a configuration dump from standard input and restore it.
`
}

func (c *restoreCommand) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&c.v2compat, "v2-compat", false, "restore an autoradio v2 backup")
}

func (c *restoreCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	decoder := json.NewDecoder(os.Stdin)
	var mounts []*pb.Mount
	if c.v2compat {
		var legacyMounts []legacyMount
		if err := decoder.Decode(&legacyMounts); err != nil {
			log.Printf("ERROR: %v", err)
			return subcommands.ExitFailure
		}
		for _, m := range legacyMounts {
			mounts = append(mounts, m.toProto())
		}
	} else {
		if err := decoder.Decode(&mounts); err != nil {
			log.Printf("ERROR: %v", err)
			return subcommands.ExitFailure
		}
	}

	client := getClient()
	errs := 0
	for _, m := range mounts {
		if err := client.SetMount(ctx, m); err != nil {
			log.Printf("ERROR: creating mount %s: %v", m.Path, err)
			errs++
		}
	}
	if errs > 0 {
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")

	subcommands.Register(&createMountCommand{}, "")
	subcommands.Register(&createTranscodingMountCommand{}, "")
	subcommands.Register(&editMountCommand{}, "")
	subcommands.Register(&deleteMountCommand{}, "")
	subcommands.Register(&listMountsCommand{}, "")
	subcommands.Register(&showMountCommand{}, "")
	subcommands.Register(&backupCommand{}, "")
	subcommands.Register(&restoreCommand{}, "")
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	os.Exit(int(subcommands.Execute(context.Background())))
}

package main

import (
	"context"
	"flag"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	_ "net/http/pprof"

	"git.autistici.org/ale/autoradio"
	"git.autistici.org/ale/autoradio/node"
	"git.autistici.org/ale/autoradio/node/icecast"
	"git.autistici.org/ale/autoradio/util"
	"go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
	"golang.org/x/sync/errgroup"
)

var (
	name          = flag.String("name", hostname(), "Name for this node (FQDN)")
	publicIPs     = util.IPListFlag("public-ip", "Public IP for this machine (may be specified more than once). If unset, the program will try to resolve the local hostname, or it will fall back to inspecting network devices.")
	peerIP        = util.IPFlag("peer-ip", "Internal IP for this machine (within the cluster), if different from --ip")
	httpPort      = flag.Int("http-port", 80, "HTTP port")
	httpsPort     = flag.Int("https-port", 443, "HTTPS port (if 0, disable HTTPS)")
	dnsPort       = flag.Int("dns-port", 53, "DNS port")
	gossipPort    = flag.Int("gossip-port", 2323, "Gossip GRPC port")
	metricsPort   = flag.Int("monitoring-port", 2424, "HTTP port for monitoring")
	bwlimit       = flag.Int("bwlimit", 100, "Bandwidth usage limit (Mbps), for load-balancing")
	maxClients    = flag.Int("max-clients", 1000, "Maximum number of connected clients, for load-balancing")
	etcdEndpoints = flag.String("etcd", "http://localhost:2379", "Etcd endpoints (comma-separated list of URLs)")

	domain      = flag.String("domain", "", "public DNS domain")
	lbSpec      = flag.String("lb-policy", "listeners_available,listeners_score,weighted", "Load balancing rules specification (see godoc documentation for details)")
	nameservers = flag.String("nameservers", "", "Comma-separated list of name servers (not IPs) for the zone specified in --domain")
	acmeEmail   = flag.String("acme-email", "", "Email address for Letsencrypt account")
	acmeNames   = flag.String("acme-cert-names", "", "Names to put on the SSL certificate (comma-separated list)")

	icecastConfigPath  = flag.String("icecast-config", "/var/lib/autoradio/icecast.xml", "Icecast configuration file")
	icecastAdminPwPath = flag.String("icecast-pwfile", "/var/lib/autoradio/.admin_pw", "Path to file with Icecast admin password")

	sessionTTL = 5
)

func hostname() string {
	hostname, _ := os.Hostname()
	return hostname
}

// Returns the list of all non-loopback addresses (IPv4 and IPv6) for
// all interfaces.
func nonLocalAddrs() []net.IP {
	var ips []net.IP

	// nolint: errcheck
	interfaces, _ := net.Interfaces()
	for _, intf := range interfaces {
		addrs, _ := intf.Addrs()
		for _, addr := range addrs {
			ip, _, err := net.ParseCIDR(addr.String())
			if err != nil || ip.IsLoopback() || ip.IsLinkLocalUnicast() {
				continue
			}
			ips = append(ips, ip)
		}
	}

	return ips
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Sanity check the configuration.
	if *name == "" {
		log.Fatal("--name must be set")
	}
	if !strings.Contains(*name, ".") && *httpsPort > 0 {
		log.Fatal("--name must be a FQDN when SSL is enabled")
	}
	if *domain == "" {
		log.Fatal("--domain must be set")
	}
	if *nameservers == "" {
		log.Fatal("--nameservers must be set")
	}
	var peerIPs []net.IP
	if err := util.DetectPublicNetworkParams(publicIPs, peerIPs, nil); err != nil {
		log.Fatal(err)
	}
	if len(*publicIPs) == 0 {
		log.Fatal("--public-ip must be set")
	}
	if *peerIP == nil {
		log.Fatal("--peer-ip must be set")
	}

	// Create the etcd client and establish the Session that will
	// control the lifecycle of the node. Enable auto-sync because
	// this is a long-lived connection.
	etcd, err := clientv3.New(clientv3.Config{
		Endpoints:        strings.Split(*etcdEndpoints, ","),
		DialTimeout:      5 * time.Second,
		AutoSyncInterval: 5 * time.Minute,
	})
	if err != nil {
		log.Fatalf("failed to connect to etcd: %v", err)
	}
	defer etcd.Close()

	// Log a message here because the next step will hang forever
	// if etcd is not reachable. An alternative would be to set a
	// timeout (with custom Context and concurrency.WithContext),
	// and rely on the service restart.
	log.Printf("establishing etcd session...")
	session, err := concurrency.NewSession(etcd, concurrency.WithTTL(sessionTTL))
	if err != nil {
		log.Fatalf("could not establish etcd session: %v", err)
	}

	// Create a top-level Context that can be canceled when the
	// program must terminate. This Context controls the lifetime
	// of the Node itself, and all the associated background
	// goroutines: canceling it immediately terminates all
	// outbound requests.
	ctx, cancel := context.WithCancel(context.Background())
	var g *errgroup.Group
	g, ctx = errgroup.WithContext(ctx)

	go func() {
		// Stop everything if the etcd session goes away.
		<-session.Done()
		log.Printf("etcd session gone, terminating...")
		cancel()
	}()

	// Set up a clean shutdown function on SIGTERM that will
	// cancel the controlling Context.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating due to signal...")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	// Create the Icecast controller.
	ice, err := icecast.NewController(ctx, autoradio.IcecastPort, *icecastConfigPath, *icecastAdminPwPath)
	if err != nil {
		log.Fatalf("could not start Icecast controller: %v", err)
	}

	// Create and start the local Node.
	bwlimitBytes := *bwlimit / 8 * 1000000
	n, err := node.New(ctx, session, ice, *name, *publicIPs, *peerIP, *gossipPort, *lbSpec, bwlimitBytes, *maxClients)
	if err != nil {
		log.Fatalf("could not initialize node: %v", err)
	}

	// Start all the network services. DNS will listen on all
	// non-loopback addresses on all interfaces, to let people run
	// a loopback cache if necessary.
	config := node.Config{
		Domain:      *domain,
		Nameservers: strings.Split(*nameservers, ","),
		DNSAddrs:    nonLocalAddrs(),
		PeerAddr:    *peerIP,
		HTTPPort:    *httpPort,
		HTTPSPort:   *httpsPort,
		DNSPort:     *dnsPort,
		GossipPort:  *gossipPort,
		IcecastPort: autoradio.IcecastPort,
		MetricsPort: *metricsPort,
		ACMEEmail:   *acmeEmail,
		CertNames:   strings.Split(*acmeNames, ","),
	}
	srv, err := node.NewServer(ctx, etcd, n, &config)
	if err != nil {
		log.Fatalf("could not initialize server: %v", err)
	}

	// Wait until the Node and the Server terminate. A failure in
	// either the network services or the Node itself should cause
	// the other to terminate.
	g.Go(func() error {
		return srv.Wait()
	})
	g.Go(func() error {
		n.Wait()
		return nil
	})
	if err := g.Wait(); err != nil && err != context.Canceled {
		log.Printf("fatal error: %v", err)
	}
}

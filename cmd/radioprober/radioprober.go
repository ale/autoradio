package main

import (
	"flag"
	"log"
	"net/http"
	_ "net/http/pprof"
	"strings"

	"git.autistici.org/ale/autoradio/prober"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type listFlag []string

func (l listFlag) String() string {
	return strings.Join(l, ",")
}

func (l *listFlag) Set(value string) error {
	*l = append(*l, value)
	return nil
}

var (
	addr = flag.String("addr", ":2525", "Address to listen on")
	urls listFlag
)

func init() {
	flag.Var(&urls, "url", "URL of the remote stream (can be specified multiple times)")
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if len(urls) == 0 {
		log.Fatal("--url must be set")
	}

	// Spawn the stream listeners.
	for _, u := range urls {
		p, err := prober.New(u)
		if err != nil {
			log.Fatal(err)
		}
		go p.Run()
	}

	// Start the HTTP listener to export Prometheus metrics.
	http.Handle("/metrics", promhttp.Handler())
	if err := http.ListenAndServe(*addr, nil); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	numConns  = flag.Int("n", 3, "number of parallel connections")
	stats     = NewStats()
	retryTime = 2 * time.Second
)

type Stats struct {
	HttpStatus map[int]int
	HttpErrors int
	Errors     int
	Underruns  int
	lock       sync.Mutex
}

func NewStats() *Stats {
	return &Stats{
		HttpStatus: make(map[int]int),
	}
}

func (s *Stats) HttpError(resp *http.Response) {
	s.lock.Lock()
	defer s.lock.Unlock()

	cur, ok := s.HttpStatus[resp.StatusCode]
	if !ok {
		cur = 0
	}
	s.HttpStatus[resp.StatusCode] = cur + 1
	s.HttpErrors++
}

func (s *Stats) Error() {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.Errors++
}

func (s *Stats) Underrun() {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.Underruns++
}

func (s *Stats) Dump() {
	s.lock.Lock()
	defer s.lock.Unlock()
	log.Printf("errs=%d underruns=%d http_errs=%d http_status=%v", s.Errors, s.Underruns, s.HttpErrors, s.HttpStatus)
}

func randomDuration(max time.Duration) time.Duration {
	return time.Duration(rand.Int63n(int64(max)))
}

func readstream(id int, streamUrl string) error {
	// Create a new client so we do not multiplex requests.
	client := &http.Client{
		Transport: &http.Transport{},
	}

	req, err := http.NewRequest("GET", streamUrl, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Connection", "close")

	resp, err := client.Do(req)
	if err != nil {
		stats.Error()
		return err
	}
	if resp.StatusCode != 200 {
		stats.HttpError(resp)
		resp.Body.Close()
		return fmt.Errorf("http status %s", resp.Status)
	}

	// Handle (very roughly) M3U files.
	if resp.Header.Get("Content-Type") == "audio/x-mpegurl" {
		data, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			stats.Error()
			return err
		}

		streamUrl = strings.TrimSpace(string(data))
		resp, err = http.Get(streamUrl)
		if err != nil {
			stats.Error()
			return err
		}
		if resp.StatusCode != 200 {
			stats.HttpError(resp)
			resp.Body.Close()
			return fmt.Errorf("http status %s", resp.Status)
		}
	}

	log.Printf("worker(%d): connected to %s", id, resp.Request.URL.String())

	defer resp.Body.Close()

	// Is it actually an audio stream?
	switch resp.Header.Get("Content-Type") {
	case "application/ogg", "audio/mpeg":
	default:
		return fmt.Errorf("unknown Content-Type: %s", resp.Header.Get("Content-Type"))
	}

	// Just read data and discard it. While reading data, attempt
	// to detect stalled connections by constantly computing a
	// bitrate approximation.
	bytes := 0
	lastStamp := time.Now()
	buf := make([]byte, 16384)
	for {
		n, err := io.ReadFull(resp.Body, buf)
		if err != nil {
			break
		}
		if n == 0 {
			break
		}

		bytes += n
		now := time.Now()
		bps := float64(n) / now.Sub(lastStamp).Seconds()
		// Ignore the first few seconds.
		if bytes > 65535 && bps < 2000 {
			stats.Underrun()
			return fmt.Errorf("bitrate too low (%g Bps)", bps)
		}
		lastStamp = now
	}
	stats.Error()
	return errors.New("connection lost")
}

func worker(id int, streamUrl string) {
	time.Sleep(randomDuration(10 * time.Second))
	for {
		err := readstream(id, streamUrl)
		log.Printf("worker(%d): %v", id, err)
		time.Sleep(randomDuration(retryTime))
	}
}

func dumpStats() {
	t := time.NewTicker(10 * time.Second)
	for {
		<-t.C
		stats.Dump()
	}
}

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Printf("Usage: radiobench [<OPTIONS>] <STREAM_URL>\n")
		os.Exit(1)
	}

	streamUrl := flag.Arg(0)

	go dumpStats()

	var wg sync.WaitGroup
	for i := 0; i < *numConns; i++ {
		wg.Add(1)
		go func(id int) {
			worker(id, streamUrl)
			wg.Done()
		}(i + 1)
	}
	wg.Wait()
}

package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"git.autistici.org/ale/autoradio/transcoder"
	"go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
)

var (
	name          = flag.String("name", shortHostname(), "Name for this node")
	etcdEndpoints = flag.String("etcd", "http://localhost:2379", "Etcd endpoints (comma-separated list of URLs)")

	sessionTTL = 5
)

func shortHostname() string {
	hostname, _ := os.Hostname()
	if r := strings.Index(hostname, "."); r >= 0 {
		return hostname[:r]
	}
	return hostname
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if *name == "" {
		log.Fatal("--name must be set")
	}

	// Create the etcd client and establish the Session that will
	// control the lifecycle of the node. Enable auto-sync because
	// this is a long-lived connection.
	etcd, err := clientv3.New(clientv3.Config{
		Endpoints:        strings.Split(*etcdEndpoints, ","),
		DialTimeout:      5 * time.Second,
		AutoSyncInterval: 5 * time.Minute,
	})
	if err != nil {
		log.Fatalf("failed to connect to etcd: %v", err)
	}
	defer etcd.Close()

	// Log a message here because the next step will hang forever
	// if etcd is not reachable. An alternative would be to set a
	// timeout (with custom Context and concurrency.WithContext),
	// and rely on the service restart.
	log.Printf("establishing etcd session...")
	session, err := concurrency.NewSession(etcd, concurrency.WithTTL(sessionTTL))
	if err != nil {
		log.Fatalf("could not establish etcd session: %v", err)
	}

	// Create a top-level Context that can be canceled when the
	// program must terminate. This Context controls the lifetime
	// of the Node itself, and all the associated background
	// goroutines: canceling it immediately terminates all
	// outbound requests.
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		// Stop everything if the etcd session goes away.
		<-session.Done()
		log.Printf("etcd session gone, terminating...")
		cancel()
	}()

	// Set up a clean shutdown function on SIGTERM that will
	// cancel the controlling Context.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating due to signal...")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	m := transcoder.New(ctx, session, *name)
	defer m.Close()

	// Wait until termination is requested via signal.
	<-ctx.Done()
}
